
# SC 3DS Max Tools (VRay)

Material and Lighting toolset to aid in recreating Star Citizen materials in 3DS Max with VRay Materials

Warning: These tools are experimental. Failures are likely. In the event of a script failure, the script most be closed and then reopened. If multiple failures occur, restarting 3DS Max may be necessary.

### Suggestion: Add an attributions/reference section for things like the following

POM uses Martin Geupel (www.racoon-artworks.de) OSL

### Editor note: definitions like this would be a good thing to put in the definitions section I recommend later

**Material Loaders:** are tools to aid in quickly recreating Star Citizen materials. These are not automated loader and require input to
create the materials. Incorrect inputs can cause the script to fail.

## Changelog

### New “Auto-Field” loader

This new feature will auto-load almost all the fields, but not all of them (some tiling information is not loaded).

The feature relies on the `.txt` file to give instructions from the Maxscript to several python scripts. So you may encounter the fulling errors if things are not done correctly.

1. Crashes when trying to load data that is not available. Requires a complete shutdown and restart of 3DS Max.

2. Accessing previous instructions from previous materials if the current material doesn't require instructions for that parameter.

3. Trying to load decal or glow/light materials will cause a light crash. Close the script and reopen it.

## Basic tutorial/documentation?

### Use Instructions

- Select MTL ID and then LOAD and select the mtl you wish to load from (this creates instructions the python script to pull that individual material data). This is a bug that needs to be worked on, eventual you'll only need to load the mtl once

### Video series

<https://www.youtube.com/playlist?list=PLr5XU4B6R4l6W5QOhHn3b7-qtATMgeiUo>

# Included Tools/Scripts

## Tools included

**General MTL Loader:** Material Loader for a majority of Star Citizen Materials including standard and blend materials

**Layer Loader (WDA):** Material Loader to create layer materials to be placed in a .mat library file to be used in a the WDA Equipment and Clothing scripts

**WDA Equipment:** WDA loader for ship components, weapons, and FPS weapons. **NOTE:** Dependent on a layer MAT file.

**WDA Clothing:** WDA loader for armor and clothing. **NOTE:** Dependent on a layer .mat file.

**POM:** Material Loader to create POM materials and POM Decals.

**Decal:** Material Loader to create Decals

**Glass:** Material Loader design to create Glass Materials

**Glow:** Material Loader to create light and glow materials using VRayLight materials

**Stencil Decal:** Material Loader to create Stencil Decals

**Pivot Align to Parent:** Use to align geometry's pivot point to the parent dummy/helper node. This obsoletes the need for the parent dummy/helper and can be deleted to clean up the scene.

**Lib Aid:** *Not currently Functional*. Goal is to have an utility to replace the Standard Material from an imported FBX model with one from a library that the user has created.

**Gamma Override:** Applies gamma override to all bitmaps. For use only with the Light Loader Standard.

**Light Loader Vray:** Loads lights from a .SCBP file into a scene. *Does not load transformations (position and rotations)*!

**Light Loader Standard:** Loads lights from a .SCBP file into a scene. Bitmaps will need gamma correction. *Does not load transformations (position and rotations)*!

## Buttons

These simple scripts are design to be used to create buttons. To use them, open the .ms file and highlight all the code and then drag and drop them on a toolbar.

**Add Dirt.ms:** Fixing tool, to add VrayDirt Layers to legacy materials

**Light_Off.ms:** Turns all selected lights OFF

**Light_On.ms:** Turns all selected lights ON

**PivotAlign2Parent.ms:** **[WIP]** Aligns selected object's pivot point to the parents (useful for removing unnecessary helpers.)

**VrayTextureActive.ms:** **[WIP]** Show texture of VrayMaterials. Currently having trouble with the varity of VRayBlendMTL setups

## Additional Tools

You'll find additional tools in the StarFabPython folder that were used in testing. These are not needed, but feel free to experiment with them.

### Editor note: find where the following goes

## Notes on Accuracy

These materials are created with solid educated guess from experience from engineering knowledge from others games. Star Citizen uses a very cleaver, but very complicated materials. As such material setups are on based on educated guess and experimentation, but may still not be 100% accurate. Additionally high end rendering programs have more values then game engines and do not need to use game engine “tricks” to maintain high frame rates. The quality over quantity approach of 3D programs renders different results. As such, I have included some more parameters that can be used to experiment with the materials (gamma, IOR, gloss boosts, and specular boosts).

# Installation

1. Unzip and place scripts and folders into your 3DS Max Script folder.

2. Open the following scripts in the StarFabPython folder and edit the texture path to where you store your textures: **Editor note: We can easily remove this step, maxscripts support global variables**

    ```text
    1. SC_DECAL.ms
    2. SC_Glass.ms
    3. SC_glow.ms
    4. SC_Layer_Importer.ms
    5. SC_MTL_Loader.ms
    6. SC_POM.ms
    7. SC_Stencil_Decal.ms
    8. SC_WDA_Clothing_Importer.ms
    9. SC_WDA_Equipment_Importer.ms
    10.StarFab_MAX_LightMaker_**.py
    ```

3. Open `SC_POM` and set the path to your version of Max

4. Open `SC_WDA_CLOTHING_IMPORTER.ms` and `SC_WDA_EQUIPMENT_IMPORTER.ms` and set the material directory once you recreate the Layer materials.

## Additional setup

- As with all my scripts you will need to adjust your paths to match up with your system setup. This includes placing a new temporary folder to hold the instructions to and from scripts.

- The scripts read the CryEngine `.mtl` in `JSON` format, so you will need to make sure you export your `.mtl` files in `JSON`. I personally export my `.mtl` files as `XML` and place them in my master file reservoir, and create separate folder containing my `.mtl` in `JSON` format. You will see this reflected in my scripts.

### Editor note: where does this go?

```text
Notes on Material Tools: These tools aid in loading material. They replace the current active material slot, so do not try loading the material while in a slot that you do not want to get reset. Note that this does not replace the current active material.
```

# Understanding Attribute Overrides vs Attribute Scales (Theory)

When reading Star Citizen `.mtl` files, you will notice that values will be listed multiple times in individual material, such as detail tiling. Understanding what is what is important to accurately recreate the materials.

### **Suggestion: Potentially put definitions here to both standardize terminology use and allow users to understand the following documentation**

## Material Overrides

Attributes listed in `<Textures>` and `<MatLayers>` are considered overrides. These values will override the base value of map or layer material.

**Example 1:**
A metal material is loaded into a WDA Cloth material and as tile value of `8.0`. The `.mtl` lists a `UVTiling=”150”`, thus the material layer changes its value from `8.0` to `150.0`.

Attributes found in the materials header or `<PublicParams>` are scalar, and multiply the base parameter by that number.

**Example 2:**
A blend material has inputted a layer in reference to a ship layer tile (these are different than the layer materials used in WDA materials). In reference to the detail scale of the layer, it lists the ~~detail tile scale~~ **Layer Detail Scale?** as `8.0` ~~This is base tile of the detail map~~ **Editor note: what is 'this'? Some things may be getting lost in translation.**.

The main `.mtl` file lists the Detail Scale (main) as `10.0`, this is the scale multiplier for the material. Using the following equation: `Layer Detail Scale [base scale] * Material Scale [main] = 8.0 * 10.0 = 80.0` Scale. So the observed **value?** in 3DS Max for `UVTile` would be `80.0`. **Editor note: This may go well with the previous note, along with some value definitions**

## Dirt_Color, Dirt_Control, Wear_Control

Using KStudios' “Instance Material by Name” script you can a**ccess?** master controls for the Dirt Color, Dirt Amount, and Wear Amount, by
instancing the Color Correction Maps. **Editor note: this may just be a note that I assigned too much value to**

## Metal Materials

Because CryEngine and 3DS Max handle metal materials differently metals will need to be loaded differently. Generally I suggest
doing the following:

1. Set Metalness to 1.
2. Set the Diffuse value in the Specular. If the metal is indicated to be polished, then diffuse = `(0,0,0)` or `(1,1,1)`;  and Specular = `(1,1,1)`.
3. Set the IOR: look at the material name to find out what IOR value to use. You can look up IOR values for different materials via internet resources. In doubt using the values of `2.16` (Titanium), `2.5` (Steel), or `1.44` (Aluminum) should suffice.

It should be noted that not every "metal" material needs a metallic effect added to it, as it also includes painted metal materials, which would have paint as the observed material.

# Tools

**Screenshot here**

## Tool Bar Menu

Located in your script folder. This script gives you fast access to

When using Material Aid Tools, it is suggested to use a fresh material, instead of reusing the same slot again and again.

## General MTL Loader

This VRayBlendMTL will cover a majority of materials in the game, specifically Blend and Illum materials. While not all materials will
use a 2 layer system, the 2nd layer can be ignored.

There are 3 major ways that Star Citizen lists out these materials in the .mtl file.

1. Single Layer with all textures paths and parameters listed in the .mtl file.

2. Dual Layer with all textures paths and parameters listed in the .mtl file.

3. Dual Layer with reference to ship layer .mtl (not to be confused with the layer files used in for equipment and components), and contain overrides and multiplies in the main .mtl

**Example:**
Dual Layer with all textures paths and parameters listed in the .mtl file

**Screenshot here**

**Example:**
Dual Layer with reference to ship layer .mtl and contain overrides and multiplies in the main .mtl and TINT layers.

**Screenshot here**

**Theory:**
Note that the Diffuse in the header is placed in the “Diffuse Layer Color” at the bottom and the Layer Descriptions Tint are placed in the Diffuse layer information. Tint info is placed in the Tint Table on the upper right corner. The Tint Name needs to be typed out, a single number is a sufficient.

I have experimented with both using the diffuse header info in the layer color and have gotten either good and bad results.
Experiment as needed. If you get bad results, ignore the header diffuse and just use the diffuse info in the the layer information.

## Artistic Options

**Boost Specular:** Alters the gamma of the specular value to increase the specular strength.

**Boost Layer Base:** Bumps the Gloss Strength of the Base Layer

**Boost Layer Blend:** Bumps the Gloss Strength of the Blend Layer

**Metal and B_Metal:** Since CryEngine doesn't use a PBR based material and doesn't include a metal values. This lets you manually control the value.

**IOR:** As above, CryEngine doesn't use IOR. This lets you manually control it. Default is 1.6 (covers most materials).

## Decal Material Loader

Material Loader in aiding of loading decals.

**Screenshot here**

## Glass Material Loader

Material Loader to aid in glass material creation. Contains pre-set Refraction IOR settings for Thin, Medium, and Thick glass. Use Thin as default for most glass materials, including windows and cockpit canopies. Medium can be used for thicker glass panel seen in kitchen cabinets or shower doors. Use Thick glass for drinking glasses.

**Screenshot here**

**Import Gladius Glass:** **[WIP]** loads the standard Gladius Glass Material with adjustments only to Diffuse, Spec, Gloss, and Dirt Color.

## Layer Loader (WDA)

In order to work with the WDA material loading tools, you will need to create a material library (`.mat`) named `SC_Layer.mat` and place the created materials in the library. It is imperative that the **NAMES of the files be EXACTLY THE SAME** or the WDA Loaders will fail to load the material and the script will fail and need to be restarted. In order to prevent user error, the prefixes are added to the name via a drop down menu.

**Create a Blank Material:** Because the WDA Loaders will require generic material with raw values (usually listed as `Path=””`) this necessitates the creation of a “Blank” via the Layer Loader that will be compatible with the WDA Material Loaders. These can also be used in placement for WDA Materials loaders that have missing layers.

## WDA Equipment Loader

Used to aid in loading WDA Equipment Materials.

Note: The WDA layer `.mat` library is required to completed **?** and Material Directory to linked to it.

Place materials as listed below:

**Screenshot here**

With “Mat Name” listed as `metal`, check `Metal` and uncheck `Tint`, see Wear Layer above.

## WDA Material Clothing Tool

Note: The WDA layer `.mat` library is required to completed **?** and Material Directory to linked to it.

Place materials as listed below:

**Screenshot here**

With “Mat Name” listed as `metal`, check `Metal` and uncheck `Tint`. See below:

**Screenshot here**

**Missing or Blank Materials**
If you get a Material listed as `Path=””`, this is standard material with no maps, just diffuse and gloss values. Use your “blank” material in place of here with the included values. If the Layer is missing, set the "blank" material with any values as the placeholder for a missing layer.

**Screenshot here**

**Creating a 2nd UVW for Decals:**
Armor, Undersuits, and some clothing use a decal layer that uses a UVW that is stored in a second channel, this layer is not exported by CGF-Converter. You will need to copy and create a 2nd UVW channel on the mesh and manually adjust the 2nd UVW channel based on the first.

Because the Decal texture isn't loaded with tiling, placing any of the the UVW coordinates outside the texture map will cause no decal to applied to the surface. Use the method below to set up your 2nd UVW channel.

1. Apply UVW unwrap to the model

2. Switch to the 2nd channel and select copy the previous UVW map

3. Open UV Editor

4. Select all faces and move them outside of the texture map

5. Using in game reference, you will need to select the faces that need a decal applied and move them to over to decal that needs to be applied. Rotation and scaling will be needed.*

6. After all decal adjustments are made, collapse the object prevent accidental adjustment of the UVW map.**

**Note: if you double click the face that you think you will need adjust and it comes back with a small group of faces and you can move them without needing to use the break tool, then you have selected the correct faces for that decal*

***Note: you may want to copy the skin modifier first and then reapply the modifier after the collapse.*

Because of the material setup, placing a decal texture in the material loader, when the `.mtl` doesn't call for one, will not have any adverse effects to the apperance.

**WDA Reset:** resets the materials in the Main Texture area.

**Material Reset:** resets the material layers. This is very useful when going creating materials for clothing.

## Common Reasons for scripts to Fail

**Input Error:** Accidentally leaving a `“` or `,` are some of the most common errors

**Missing Layer:** If one of the WDA Material Importers fails and missing an input it is usually due to a missing Layer Material. The `.mtl` files will occasionally list Layer Materials that do not exist. Either they were not created which can be fixed by importing from the `.mtl`, there is an error and the file needs to substitute in the loader OR you can create a duplicate of the correct file with the missing name.

**Known missing Layer Materials:**

1. highresolution/natural/cotton_01.mtl
*fix: lowresolution/natural/cotton_01.mtl*

2. highresolution/natural/cotton_dirty_01.mtl
*fix: lowresolution/natural/cotton_dirty.mtl*

3. lowresolution/synthetic/rubber_diamond_01.mtl
*fix: lowresolution/synthetic/rubber_diamond_02.mtl*

## Parallax Occlusion Map Loader

**WARNING: You must open the `.ms` file and edit the pathway location of where the `SCM.mat` file is located. It is normally located at `/scripts/StarFabPython/`, but you will need to edit the path before that to your Max version location.**

Two versions of Parallax Occlusion Map Materials exist:

1. Normal Materials with POM effects

2. POM Decals (used to give lines in panels)

### Normal Material with POM Effect

**Screenshot here**

### POM Decal

To make a POM decal. Check `POM DECAL` and make sure `Opacity` is **UNCHECKED**.

### Light Loaders

**Light Loader Vray:** Loads lights from a `.SCBP` file into a scene.*

**Light Loader Standard:** Loads lights from a `.SCBP` file into a scene. Bitmaps will need gamma correction.*  

**Does not load transformations (position and rotations)*

**Warning:** the Light Loader UI is unstable, I recommend running the python scripts via the run script menu and keep the UI open to help quickly save the files.

The Light Loaders only load the details of the light parameters, but none of the transforms! To use them, follow these steps:

1. Run the either the VRay or Standard Default light importer

2. Save the Default light scene

3. Clear the scene or delete all the lights

4. Run the either the VRay or Standard Aux light importer

5. Save the Aux light scene

6. Clear the scene or delete all the lights

7. Run the either the VRay or Standard Emergency light importer

8. Save the Emergency light scene

9. Clear the scene or delete all the lights

10. Load your main ship file

11. `File->Import->Replace->”Import_Light_AUX.max"`

12. Replace all the lights in the scene

13. “CTRL+V” and make duplicates of all the highlighted lights

14. While the new lights are highlighted, Place them into a new layer.

15. Rename these lights `*_aux`

16. Repeat for Emergency lights

17. When importing the default lights, DO NOT duplicate them. Replace them and put them into a new layer.
