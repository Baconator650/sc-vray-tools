import json
import pymxs #MaxScript Wrapper  
from pymxs import runtime as rt 


filemtl=open("T:/SC_Cache/TintInput.txt", 'r') #Set to your temp folder
Base_MTL_Load=str(filemtl.read())
filemtl.close()
Base_MTL_Load = Base_MTL_Load.replace("\n",'')
Base_MTL_Load = Base_MTL_Load.replace('\\', '/')
Base_MTL_Load = Base_MTL_Load.replace('"', '')

print(Base_MTL_Load)


SC_L =json.load(open(Base_MTL_Load))



DecalColorR_R=str(SC_L ['root']['decalColorR'].get('r'))
DecalColorR_G=str(SC_L ['root']['decalColorR'].get('g'))
DecalColorR_B=str(SC_L ['root']['decalColorR'].get('b'))

DecalColorG_R=str(SC_L ['root']['decalColorG'].get('r'))
DecalColorG_G=str(SC_L ['root']['decalColorG'].get('g'))
DecalColorG_B=str(SC_L ['root']['decalColorG'].get('b'))

DecalColorB_R=str(SC_L ['root']['decalColorB'].get('r'))
DecalColorB_G=str(SC_L ['root']['decalColorB'].get('g'))
DecalColorB_B=str(SC_L ['root']['decalColorB'].get('b'))

try:
    DecalText= SC_L ['root']['decalTexture']
except KeyError:
    DecalText=None


Tint1_C_R=str(SC_L ['root']['entryA']['tintColor'].get('r'))
Tint1_C_G=str(SC_L ['root']['entryA']['tintColor'].get('g'))
Tint1_C_B=str(SC_L ['root']['entryA']['tintColor'].get('b'))
Tint1_S_R=str(SC_L ['root']['entryA']['specColor'].get('r'))
Tint1_S_G=str(SC_L ['root']['entryA']['specColor'].get('g'))
Tint1_S_B=str(SC_L ['root']['entryA']['specColor'].get('b'))
Tint1_G=str(SC_L ['root']['entryA'].get('glossiness'))

Tint2_C_R=str(SC_L ['root']['entryB']['tintColor'].get('r'))
Tint2_C_G=str(SC_L ['root']['entryB']['tintColor'].get('g'))
Tint2_C_B=str(SC_L ['root']['entryB']['tintColor'].get('b'))
Tint2_S_R=str(SC_L ['root']['entryB']['specColor'].get('r'))
Tint2_S_G=str(SC_L ['root']['entryB']['specColor'].get('g'))
Tint2_S_B=str(SC_L ['root']['entryB']['specColor'].get('b'))
Tint2_G=str(SC_L ['root']['entryB'].get('glossiness'))

Tint3_C_R=str(SC_L ['root']['entryC']['tintColor'].get('r'))
Tint3_C_G=str(SC_L ['root']['entryC']['tintColor'].get('g'))
Tint3_C_B=str(SC_L ['root']['entryC']['tintColor'].get('b'))
Tint3_S_R=str(SC_L ['root']['entryC']['specColor'].get('r'))
Tint3_S_G=str(SC_L ['root']['entryC']['specColor'].get('g'))
Tint3_S_B=str(SC_L ['root']['entryC']['specColor'].get('b'))
Tint3_G=str(SC_L ['root']['entryC'].get('glossiness'))

GlassColor_R=str(SC_L ['root']['glassColor'].get('r'))
GlassColor_G=str(SC_L ['root']['glassColor'].get('g'))
GlassColor_B=str(SC_L ['root']['glassColor'].get('b'))


ZExport=['DecalColorR_R=',(DecalColorR_R or ""),'\n',
'DecalColorR_G=',(DecalColorR_G or ""),'\n',
'DecalColorR_B=',(DecalColorR_B or ""),'\n',
'DecalColorG_R=',(DecalColorG_R or ""),'\n',
'DecalColorG_G=',(DecalColorG_G or ""),'\n',
'DecalColorG_B=',(DecalColorG_B or ""),'\n',
'DecalColorB_R=',(DecalColorB_R or ""),'\n',
'DecalColorB_G=',(DecalColorB_G or ""),'\n',
'DecalColorB_B=',(DecalColorB_B or ""),'\n',
'DecalText="',(DecalText or ""),'"\n',
'Tint1_C_R=',(Tint1_C_R or ""),'\n',
'Tint1_C_G=',(Tint1_C_G or ""),'\n',
'Tint1_C_B=',(Tint1_C_B or ""),'\n',
'Tint1_S_R=',(Tint1_S_R or ""),'\n',
'Tint1_S_G=',(Tint1_S_G or ""),'\n',
'Tint1_S_B=',(Tint1_S_B or ""),'\n',
'Tint1_G=',(Tint1_G or ""),'\n',
'Tint2_C_R=',(Tint2_C_R or ""),'\n',
'Tint2_C_G=',(Tint2_C_G or ""),'\n',
'Tint2_C_B=',(Tint2_C_B or ""),'\n',
'Tint2_S_R=',(Tint2_S_R or ""),'\n',
'Tint2_S_G=',(Tint2_S_G or ""),'\n',
'Tint2_S_B=',(Tint2_S_B or ""),'\n',
'Tint2_G=',(Tint2_G or ""),'\n',
'Tint3_C_R=',(Tint3_C_R or ""),'\n',
'Tint3_C_G=',(Tint3_C_G or ""),'\n',
'Tint3_C_B=',(Tint3_C_B or ""),'\n',
'Tint3_S_R=',(Tint3_S_R or ""),'\n',
'Tint3_S_G=',(Tint3_S_G or ""),'\n',
'Tint3_S_B=',(Tint3_S_B or ""),'\n',
'Tint3_G=',(Tint3_G or ""),'\n',
'GlassColor_R=',(GlassColor_R or ""),'\n',
'GlassColor_G=',(GlassColor_G or ""),'\n',
'GlassColor_B=',(GlassColor_B or ""),'\n'
]


Layer1Output = open('T:/SC_Cache/TintOutput.txt', 'w')#Change to your local temp 
Layer1Output.writelines(ZExport)
Layer1Output.close()

filemtl.close()

gc.collect()
