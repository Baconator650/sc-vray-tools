try (closeRolloutFloater LayerAdjuster) catch()


rollout LayerAdjuster "SC WDA Layer Adjuster Tool beta 0.1.0" width:550 height:645
(
	bitmap 'bmp1' "Bitmap" pos:[81,10] width:384 height:40 fileName:"WDA_LayerAdjuster.tif" align:#left
	groupBox 'grp1' "Layer Selector" pos:[28,87] width:487 height:92 align:#left
	dropDownList 'LayerSelect' "" pos:[178,107] width:180 height:21 items:#("Layer1", "Layer2", "Layer3", "Layer4", "Wear1", "Wear2", "Wear3", "Wear4") align:#left selection:1
	label 'lbl1' "Warning: Only select baseMtl and coatMtl[1] for equipment" pos:[48,150] width:422 height:17 align:#left
	groupBox 'grp2' "Tile Adjustment" pos:[30,180] width:483 height:273 align:#left
	spinner 'Tile_Scale' "Tile Scale" pos:[168,202] width:89 height:16 range:[0,200,1] scale:0.1 align:#left
	groupBox 'grp3' "Map Adjustment" pos:[37,232] width:469 height:164 align:#left

	groupBox 'grp4' "Diff Tile" pos:[68,252] width:397 height:48 align:#left
	groupBox 'grp5' "DDNA tile" pos:[68,300] width:397 height:48 align:#left
	spinner 'Diff_TileU' "Tile U" pos:[120,269] width:74 height:16 range:[0,100,8] align:#left
	spinner 'Diff_TileV' "Tile V" pos:[284,269] width:75 height:16 range:[0,100,8] align:#left
	spinner 'DDNA_TileU' "Tile U" pos:[120,319] width:74 height:16 range:[0,100,8] align:#left
	spinner 'DDNA_TileV' "Tile V" pos:[284,319] width:75 height:16 range:[0,100,8] align:#left
	groupBox 'grp6' "Layer Multiplier" pos:[30,465] width:483 height:161 align:#left
	button 'Gloss_Increase' "Max Gloss" pos:[316,487] width:125 height:32 align:#left
	button 'Gloss_Decrease' "Remove Gloss" pos:[316,586] width:125 height:32 align:#left
	button 'Diff_Increase' "Max Diff" pos:[91,489] width:125 height:32 align:#left
	button 'Diff_Decrease' "Remove Diff" pos:[91,583] width:125 height:32 align:#left
	button 'Gloss50' "Gloss 50" pos:[316,533] width:125 height:32 align:#left
	button 'Diff50' "Diff 50" pos:[91,535] width:125 height:32 align:#left
	button 'btn_ConvertDifftoDDNA' "DIFF->DDNA Tile" pos:[61,403] width:182 height:40 align:#left
	button 'btn_ConvertDDNAtoDiff' "DDNA->DIFF Tile" pos:[300,403] width:182 height:40 align:#left
	button 'btn_set_Dtile' "Set Diff Tile" pos:[60,355] width:150 height:34 align:#left
	button 'btn_set_DNAtile' "Set DDNA Tile" pos:[300,355] width:150 height:34 align:#left
		

on Gloss_Increase pressed do 
(
	
	if LayerSelect.selected=="Layer1" do
(
	meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2]=ColorCorrection()
	meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].color=255 255 255 255
	meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].blendmode[2]=5
	meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].opacity[2]=meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.opacity[1]
)

	if LayerSelect.selected=="Layer2" do
(
	meditMaterials[activeMeditSlot].coatmtl[1].texmap_reflectionGlossiness.maplist[1].maplist[2]=ColorCorrection()
	meditMaterials[activeMeditSlot].coatmtl[1].texmap_reflectionGlossiness.maplist[1].maplist[2].color=255 255 255 255
	meditMaterials[activeMeditSlot].coatmtl[1].texmap_reflectionGlossiness.maplist[1].blendmode[2]=5
	meditMaterials[activeMeditSlot].coatmtl[1].texmap_reflectionGlossiness.maplist[1].opacity[2]=meditMaterials[activeMeditSlot].coatmtl[1].texmap_reflectionGlossiness.opacity[1]
)

	if LayerSelect.selected=="Layer3" do
(
	meditMaterials[activeMeditSlot].coatmtl[2].texmap_reflectionGlossiness.maplist[1].maplist[2]=ColorCorrection()
	meditMaterials[activeMeditSlot].coatmtl[2].texmap_reflectionGlossiness.maplist[1].maplist[2].color=255 255 255 255
	meditMaterials[activeMeditSlot].coatmtl[2].texmap_reflectionGlossiness.maplist[1].blendmode[2]=5
	meditMaterials[activeMeditSlot].coatmtl[2].texmap_reflectionGlossiness.maplist[1].opacity[2]=meditMaterials[activeMeditSlot].coatmtl[2].texmap_reflectionGlossiness.opacity[1]
)

	if LayerSelect.selected=="Layer4" do
(
	meditMaterials[activeMeditSlot].coatmtl[3].texmap_reflectionGlossiness.maplist[1].maplist[2]=ColorCorrection()
	meditMaterials[activeMeditSlot].coatmtl[3].texmap_reflectionGlossiness.maplist[1].maplist[2].color=255 255 255 255
	meditMaterials[activeMeditSlot].coatmtl[3].texmap_reflectionGlossiness.maplist[1].blendmode[2]=5
	meditMaterials[activeMeditSlot].coatmtl[3].texmap_reflectionGlossiness.maplist[1].opacity[2]=meditMaterials[activeMeditSlot].coatmtl[3].texmap_reflectionGlossiness.opacity[1]
)

	if LayerSelect.selected=="Wear1" do
(
	meditMaterials[activeMeditSlot].coatmtl[4].texmap_reflectionGlossiness.maplist[1].maplist[2]=ColorCorrection()
	meditMaterials[activeMeditSlot].coatmtl[4].texmap_reflectionGlossiness.maplist[1].maplist[2].color=255 255 255 255
	meditMaterials[activeMeditSlot].coatmtl[4].texmap_reflectionGlossiness.maplist[1].blendmode[2]=5
	meditMaterials[activeMeditSlot].coatmtl[4].texmap_reflectionGlossiness.maplist[1].opacity[2]=meditMaterials[activeMeditSlot].coatmtl[4].texmap_reflectionGlossiness.opacity[1]
)

	if LayerSelect.selected=="Wear2" do
(
	meditMaterials[activeMeditSlot].coatmtl[5].texmap_reflectionGlossiness.maplist[1].maplist[2]=ColorCorrection()
	meditMaterials[activeMeditSlot].coatmtl[5].texmap_reflectionGlossiness.maplist[1].maplist[2].color=255 255 255 255
	meditMaterials[activeMeditSlot].coatmtl[5].texmap_reflectionGlossiness.maplist[1].blendmode[2]=5
	meditMaterials[activeMeditSlot].coatmtl[5].texmap_reflectionGlossiness.maplist[1].opacity[2]=meditMaterials[activeMeditSlot].coatmtl[5].texmap_reflectionGlossiness.opacity[1]
)

	if LayerSelect.selected=="Wear3" do
(
	meditMaterials[activeMeditSlot].coatmtl[6].texmap_reflectionGlossiness.maplist[1].maplist[2]=ColorCorrection()
	meditMaterials[activeMeditSlot].coatmtl[6].texmap_reflectionGlossiness.maplist[1].maplist[2].color=255 255 255 255
	meditMaterials[activeMeditSlot].coatmtl[6].texmap_reflectionGlossiness.maplist[1].blendmode[2]=5
	meditMaterials[activeMeditSlot].coatmtl[6].texmap_reflectionGlossiness.maplist[1].opacity[2]=meditMaterials[activeMeditSlot].coatmtl[6].texmap_reflectionGlossiness.opacity[1]
)

	if LayerSelect.selected=="Wear4" do
(
	meditMaterials[activeMeditSlot].coatmtl[7].texmap_reflectionGlossiness.maplist[1].maplist[2]=ColorCorrection()
	meditMaterials[activeMeditSlot].coatmtl[7].texmap_reflectionGlossiness.maplist[1].maplist[2].color=255 255 255 255
	meditMaterials[activeMeditSlot].coatmtl[7].texmap_reflectionGlossiness.maplist[1].blendmode[2]=5
	meditMaterials[activeMeditSlot].coatmtl[7].texmap_reflectionGlossiness.maplist[1].opacity[2]=meditMaterials[activeMeditSlot].coatmtl[7].texmap_reflectionGlossiness.opacity[1]
)


)

on Gloss_Decrease pressed do 
(
			if LayerSelect.selected=="Layer1" do
			(
				meditMaterials[activeMeditSlot].baseMtL.texmap_reflectionGlossiness.maplist[1].maplist[2]= undefined
			)
				if LayerSelect.selected=="Layer2" do
			(
				meditMaterials[activeMeditSlot].coatmtl[1].texmap_reflectionGlossiness.maplist[1].maplist[2]= undefined
			)

				if LayerSelect.selected=="Layer3" do
			(
				meditMaterials[activeMeditSlot].coatmtl[2].texmap_reflectionGlossiness.maplist[1].maplist[2]= undefined
			)

				if LayerSelect.selected=="Layer4" do
			(
				meditMaterials[activeMeditSlot].coatmtl[3].texmap_reflectionGlossiness.maplist[1].maplist[2]= undefined
			)

				if LayerSelect.selected=="Wear1" do
			(
				meditMaterials[activeMeditSlot].coatmtl[4].texmap_reflectionGlossiness.maplist[1].maplist[2]= undefined
			)

				if LayerSelect.selected=="Wear2" do
			(
				meditMaterials[activeMeditSlot].coatmtl[5].texmap_reflectionGlossiness.maplist[1].maplist[2]= undefined
			)

				if LayerSelect.selected=="Wear3" do
			(
				meditMaterials[activeMeditSlot].coatmtl[6].texmap_reflectionGlossiness.maplist[1].maplist[2]= undefined
			)

				if LayerSelect.selected=="Wear4" do
			(
				meditMaterials[activeMeditSlot].coatmtl[7].texmap_reflectionGlossiness.maplist[1].maplist[2]= undefined
			)
	
)
on Diff_Increase pressed do
(
	if LayerSelect.selected=="Layer1" do
(
	meditMaterials[activeMeditSlot].baseMtL.texmap_diffuse.maplist[1].maplist[2]=ColorCorrection()
	meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1].maplist[2].color=255 255 255 255
	meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1].blendmode[2]=5
	meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1].opacity[2]=meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.opacity[1]
)

	if LayerSelect.selected=="Layer2" do
(
	meditMaterials[activeMeditSlot].coatmtl[1].texmap_diffuse.maplist[1].maplist[2]=ColorCorrection()
	meditMaterials[activeMeditSlot].coatmtl[1].texmap_diffuse.maplist[1].maplist[2].color=255 255 255 255
	meditMaterials[activeMeditSlot].coatmtl[1].texmap_diffuse.maplist[1].blendmode[2]=5
	meditMaterials[activeMeditSlot].coatmtl[1].text.texmap_diffuse.maplist[1].opacity[2]=meditMaterials[activeMeditSlot].coatmtl[1].texmap_diffuse.opacity[1]
)

	if LayerSelect.selected=="Layer3" do
(
	meditMaterials[activeMeditSlot].coatmtl[2].texmap_diffuse.maplist[1].maplist[2]=ColorCorrection()
	meditMaterials[activeMeditSlot].coatmtl[2].texmap_diffuse.maplist[1].maplist[2].color=255 255 255 255
	meditMaterials[activeMeditSlot].coatmtl[2].texmap_diffuse.maplist[1].blendmode[2]=5
	meditMaterials[activeMeditSlot].coatmtl[2].texmap_diffuse.maplist[1].opacity[2]=meditMaterials[activeMeditSlot].coatmtl[2].texmap_diffuse.opacity[1]
)

	if LayerSelect.selected=="Layer4" do
(
	meditMaterials[activeMeditSlot].coatmtl[3].texmap_diffuse.maplist[1].maplist[2]=ColorCorrection()
	meditMaterials[activeMeditSlot].coatmtl[3].texmap_diffuse.maplist[1].maplist[2].color=255 255 255 255
	meditMaterials[activeMeditSlot].coatmtl[3].texmap_diffuse.maplist[1].blendmode[2]=5
	meditMaterials[activeMeditSlot].coatmtl[3].texmap_diffuse.maplist[1].opacity[2]=meditMaterials[activeMeditSlot].coatmtl[3].texmap_diffuse.opacity[1]
)

	if LayerSelect.selected=="Wear1" do
(
	meditMaterials[activeMeditSlot].coatmtl[4].texmap_diffuse.maplist[1].maplist[2]=ColorCorrection()
	meditMaterials[activeMeditSlot].coatmtl[4].texmap_diffuse.maplist[1].maplist[2].color=255 255 255 255
	meditMaterials[activeMeditSlot].coatmtl[4].texmap_diffuse.maplist[1].blendmode[2]=5
	meditMaterials[activeMeditSlot].coatmtl[4].texmap_diffuse.maplist[1].opacity[2]=meditMaterials[activeMeditSlot].coatmtl[4].texmap_diffuse.opacity[1]
)

	if LayerSelect.selected=="Wear2" do
(
	meditMaterials[activeMeditSlot].coatmtl[5].texmap_diffuse.maplist[1].maplist[2]=ColorCorrection()
	meditMaterials[activeMeditSlot].coatmtl[5].texmap_diffuse.maplist[1].maplist[2].color=255 255 255 255
	meditMaterials[activeMeditSlot].coatmtl[5].texmap_diffuse.maplist[1].blendmode[2]=5
	meditMaterials[activeMeditSlot].coatmtl[5].texmap_diffuse.maplist[1].opacity[2]=meditMaterials[activeMeditSlot].coatmtl[5].texmap_diffuse.opacity[1]
)

	if LayerSelect.selected=="Wear3" do
(
	meditMaterials[activeMeditSlot].coatmtl[6].texmap_diffuse.maplist[1].maplist[2]=ColorCorrection()
	meditMaterials[activeMeditSlot].coatmtl[6].texmap_diffuse.maplist[1].maplist[2].color=255 255 255 255
	meditMaterials[activeMeditSlot].coatmtl[6].texmap_diffuse.maplist[1].blendmode[2]=5
	meditMaterials[activeMeditSlot].coatmtl[6].texmap_diffuse.maplist[1].opacity[2]=meditMaterials[activeMeditSlot].coatmtl[6].texmap_diffuse.opacity[1]
)

	if LayerSelect.selected=="Wear4" do
(
	meditMaterials[activeMeditSlot].coatmtl[7].texmap_diffuse.maplist[1].maplist[2]=ColorCorrection()
	meditMaterials[activeMeditSlot].coatmtl[7].texmap_diffuse.maplist[1].maplist[2].color=255 255 255 255
	meditMaterials[activeMeditSlot].coatmtl[7].texmap_diffuse.maplist[1].blendmode[2]=5
	meditMaterials[activeMeditSlot].coatmtl[7].texmap_diffuse.maplist[1].opacity[2]=meditMaterials[activeMeditSlot].coatmtl[7].texmap_diffuse.opacity[1]
)
)

on Diff_Decrease pressed do
(
				if LayerSelect.selected=="Layer1" do
		(
			meditMaterials[activeMeditSlot].baseMtL.texmap_diffuse.maplist[1].maplist[2]= undefined
		)
			if LayerSelect.selected=="Layer2" do
		(
			meditMaterials[activeMeditSlot].coatmtl[1].texmap_diffuse.maplist[1].maplist[2]= undefined
		)

			if LayerSelect.selected=="Layer3" do
		(
			meditMaterials[activeMeditSlot].coatmtl[2].texmap_diffuse.maplist[1].maplist[2]= undefined
		)

			if LayerSelect.selected=="Layer4" do
		(
			meditMaterials[activeMeditSlot].coatmtl[3].texmap_diffuse.maplist[1].maplist[2]= undefined
		)

			if LayerSelect.selected=="Wear1" do
		(
			meditMaterials[activeMeditSlot].coatmtl[4].texmap_diffuse.maplist[1].maplist[2]= undefined
		)

			if LayerSelect.selected=="Wear2" do
		(
			meditMaterials[activeMeditSlot].coatmtl[5].texmap_diffuse.maplist[1].maplist[2]= undefined
		)

			if LayerSelect.selected=="Wear3" do
		(
			meditMaterials[activeMeditSlot].coatmtl[6].texmap_diffuse.maplist[1].maplist[2]= undefined
		)

			if LayerSelect.selected=="Wear4" do
		(
			meditMaterials[activeMeditSlot].coatmtl[7].texmap_diffuse.maplist[1].maplist[2]= undefined
		)
)

on Gloss50 pressed do
(
					if LayerSelect.selected=="Layer1" do
				(
					meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].opacity[2]=50
				)

					if LayerSelect.selected=="Layer2" do
				(
					meditMaterials[activeMeditSlot].coatmtl[1].texmap_reflectionGlossiness.maplist[1].opacity[2]=50
				)

					if LayerSelect.selected=="Layer3" do
				(

					meditMaterials[activeMeditSlot].coatmtl[2].texmap_reflectionGlossiness.maplist[1].opacity[2]=50
				)

					if LayerSelect.selected=="Layer4" do
				(

					meditMaterials[activeMeditSlot].coatmtl[3].texmap_reflectionGlossiness.maplist[1].opacity[2]=50
				)

					if LayerSelect.selected=="Wear1" do
				(

					meditMaterials[activeMeditSlot].coatmtl[4].texmap_reflectionGlossiness.maplist[1].opacity[2]=50
				)

					if LayerSelect.selected=="Wear2" do
				(

					meditMaterials[activeMeditSlot].coatmtl[5].texmap_reflectionGlossiness.maplist[1].opacity[2]=50
				)

					if LayerSelect.selected=="Wear3" do
				(

					meditMaterials[activeMeditSlot].coatmtl[6].texmap_reflectionGlossiness.maplist[1].opacity[2]=50
				)

					if LayerSelect.selected=="Wear4" do
				(

					meditMaterials[activeMeditSlot].coatmtl[7].texmap_reflectionGlossiness.maplist[1].opacity[2]=50		
				)
)

on Diff50 pressed do
(
	
				if LayerSelect.selected=="Layer1" do
			(
				meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1].opacity[2]=50
			)

				if LayerSelect.selected=="Layer2" do
			(
				meditMaterials[activeMeditSlot].coatmtl[1].texmap_diffuse.maplist[1].opacity[2]=50
			)

				if LayerSelect.selected=="Layer3" do
			(

				meditMaterials[activeMeditSlot].coatmtl[2].texmap_diffuse.maplist[1].opacity[2]=50
			)

				if LayerSelect.selected=="Layer4" do
			(

				meditMaterials[activeMeditSlot].coatmtl[3].texmap_diffuse.maplist[1].opacity[2]=50
			)

				if LayerSelect.selected=="Wear1" do
			(

				meditMaterials[activeMeditSlot].coatmtl[4].texmap_diffuse.maplist[1].opacity[2]=50
			)

				if LayerSelect.selected=="Wear2" do
			(

				meditMaterials[activeMeditSlot].coatmtl[5].texmap_diffuse.maplist[1].opacity[2]=50
			)

				if LayerSelect.selected=="Wear3" do
			(

				meditMaterials[activeMeditSlot].coatmtl[6].texmap_diffuse.maplist[1].opacity[2]=50
			)

				if LayerSelect.selected=="Wear4" do
			(

				meditMaterials[activeMeditSlot].coatmtl[7].texmap_diffuse.maplist[1].opacity[2]=50		
			)
	
	
	
)

on btn_ConvertDifftoDDNA pressed do
(
	if LayerSelect.selected=="Layer1" do
(
	meditMaterials[activeMeditSlot].basemtl.texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling = meditMaterials[activeMeditSlot].basemtl.texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].basemtl.texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling = meditMaterials[activeMeditSlot].basemtl.texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling
	meditMaterials[activeMeditSlot].basemtl.texmap_bump.bump_map.coords.U_Tiling =meditMaterials[activeMeditSlot].basemtl.texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].basemtl.texmap_bump.bump_map.coords.V_Tiling =meditMaterials[activeMeditSlot].basemtl.texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling
)
	if LayerSelect.selected=="Layer2" do
(
	meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling = meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling = meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling
	meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.bump_map.coords.U_Tiling =meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.bump_map.coords.V_Tiling =meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling
)

	if LayerSelect.selected=="Layer3" do
(
	meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling = meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling = meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling
	meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump.bump_map.coords.U_Tiling =meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump.bump_map.coords.V_Tiling =meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling
)

	if LayerSelect.selected=="Layer4" do
(
	meditMaterials[activeMeditSlot].coatMtl[3].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling = meditMaterials[activeMeditSlot].coatMtl[3].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[3].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling = meditMaterials[activeMeditSlot].coatMtl[3].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling
	meditMaterials[activeMeditSlot].coatMtl[3].texmap_bump.bump_map.coords.U_Tiling =meditMaterials[activeMeditSlot].coatMtl[3].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[3].texmap_bump.bump_map.coords.V_Tiling =meditMaterials[activeMeditSlot].coatMtl[3].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling
)

	if LayerSelect.selected=="Wear1" do
(
	meditMaterials[activeMeditSlot].coatMtl[4].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling = meditMaterials[activeMeditSlot].coatMtl[4].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[4].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling = meditMaterials[activeMeditSlot].coatMtl[4].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling
	meditMaterials[activeMeditSlot].coatMtl[4].texmap_bump.bump_map.coords.U_Tiling =meditMaterials[activeMeditSlot].coatMtl[4].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[4].texmap_bump.bump_map.coords.V_Tiling =meditMaterials[activeMeditSlot].coatMtl[4].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling
)

	if LayerSelect.selected=="Wear2" do
(
	
	meditMaterials[activeMeditSlot].coatMtl[5].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling = meditMaterials[activeMeditSlot].coatMtl[5].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[5].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling = meditMaterials[activeMeditSlot].coatMtl[5].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling
	meditMaterials[activeMeditSlot].coatMtl[5].texmap_bump.bump_map.coords.U_Tiling=meditMaterials[activeMeditSlot].coatMtl[5].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[5].texmap_bump.bump_map.coords.V_Tiling=meditMaterials[activeMeditSlot].coatMtl[5].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling
	
)
	if LayerSelect.selected=="Wear3" do
(
	meditMaterials[activeMeditSlot].coatMtl[6].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling = meditMaterials[activeMeditSlot].coatMtl[6].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[6].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling = meditMaterials[activeMeditSlot].coatMtl[6].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling
	meditMaterials[activeMeditSlot].coatMtl[6].texmap_bump.bump_map.coords.U_Tiling =meditMaterials[activeMeditSlot].coatMtl[6].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[6].texmap_bump.bump_map.coords.V_Tiling =meditMaterials[activeMeditSlot].coatMtl[6].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling	
)
	if LayerSelect.selected=="Wear4" do
(
	meditMaterials[activeMeditSlot].coatMtl[7].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling = meditMaterials[activeMeditSlot].coatMtl[7].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[7].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling = meditMaterials[activeMeditSlot].coatMtl[7].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling
	meditMaterials[activeMeditSlot].coatMtl[7].texmap_bump.bump_map.coords.U_Tiling =meditMaterials[activeMeditSlot].coatMtl[7].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[7].texmap_bump.bump_map.coords.V_Tiling =meditMaterials[activeMeditSlot].coatMtl[7].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling
)

	
)	

on btn_ConvertDDNAtoDiff pressed do
(
		if LayerSelect.selected=="Layer1" do
(
	meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling=meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling=meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling 
)
	if LayerSelect.selected=="Layer2" do
(
	meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling=meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling=meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling 
	
)
	if LayerSelect.selected=="Layer3" do
(
	meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling=meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling=meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling 
	
)

	if LayerSelect.selected=="Layer4" do
(
	meditMaterials[activeMeditSlot].coatMtl[3].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling=meditMaterials[activeMeditSlot].coatMtl[3].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[3].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling=meditMaterials[activeMeditSlot].coatMtl[3].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling 
	
)

	if LayerSelect.selected=="Wear1" do
(
	meditMaterials[activeMeditSlot].coatMtl[4].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling=meditMaterials[activeMeditSlot].coatMtl[4].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[4].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling=meditMaterials[activeMeditSlot].coatMtl[4].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling 
)

	if LayerSelect.selected=="Wear2" do
(
	
	meditMaterials[activeMeditSlot].coatMtl[5].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling=meditMaterials[activeMeditSlot].coatMtl[5].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[5].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling=meditMaterials[activeMeditSlot].coatMtl[5].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling 
	
)
	if LayerSelect.selected=="Wear3" do
(
	meditMaterials[activeMeditSlot].coatMtl[6].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling=meditMaterials[activeMeditSlot].coatMtl[6].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[6].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling=meditMaterials[activeMeditSlot].coatMtl[6].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling 
)
	if LayerSelect.selected=="Wear4" do
	(
	meditMaterials[activeMeditSlot].coatMtl[7].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling=meditMaterials[activeMeditSlot].coatMtl[7].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling
	meditMaterials[activeMeditSlot].coatMtl[7].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling=meditMaterials[activeMeditSlot].coatMtl[7].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling 
	)


)

on btn_set_Dtile pressed do
(
		if LayerSelect.selected=="Layer1" do
(
	meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling=Diff_TileU.value
	meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling=Diff_TileV.value
)
	if LayerSelect.selected=="Layer2" do
(
	meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling=Diff_TileU.value
	meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling=Diff_TileV.value
	
)
	if LayerSelect.selected=="Layer3" do
(
	meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling=Diff_TileU.value
	meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling=Diff_TileV.value 
	
)

	if LayerSelect.selected=="Layer4" do
(
	meditMaterials[activeMeditSlot].coatMtl[3].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling=Diff_TileU.value
	meditMaterials[activeMeditSlot].coatMtl[3].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling=Diff_TileV.value
)

	if LayerSelect.selected=="Wear1" do
(
	meditMaterials[activeMeditSlot].coatMtl[4].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling=Diff_TileU.value
	meditMaterials[activeMeditSlot].coatMtl[4].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling=Diff_TileV.value 
)

	if LayerSelect.selected=="Wear2" do
(
	
	meditMaterials[activeMeditSlot].coatMtl[5].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling=Diff_TileU.value
	meditMaterials[activeMeditSlot].coatMtl[5].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling=Diff_TileV.value
	
)
	if LayerSelect.selected=="Wear3" do
(
	meditMaterials[activeMeditSlot].coatMtl[6].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling=Diff_TileU.value
	meditMaterials[activeMeditSlot].coatMtl[6].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling=Diff_TileV.value 
)
	if LayerSelect.selected=="Wear4" do
	(
	meditMaterials[activeMeditSlot].coatMtl[7].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling=Diff_TileU.value
	meditMaterials[activeMeditSlot].coatMtl[7].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling=Diff_TileV.value
	)	
	
	
)

on btn_set_DNAtile pressed do
(
			if LayerSelect.selected=="Layer1" do
			(
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling=DDNA_TileU.value
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling=DDNA_TileV.value
				meditMaterials[activeMeditSlot].basemtl.texmap_bump.bump_map.coords.U_Tiling = DDNA_TileU.value
				meditMaterials[activeMeditSlot].basemtl.texmap_bump.bump_map.coords.V_Tiling = DDNA_TileV.value
			)
				if LayerSelect.selected=="Layer2" do
			(
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling=DDNA_TileU.value
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling=DDNA_TileV.value
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.bump_map.coords.U_Tiling = DDNA_TileU.value
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.bump_map.coords.V_Tiling = DDNA_TileV.value
			)
				if LayerSelect.selected=="Layer3" do
			(
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling=DDNA_TileU.value
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling=DDNA_TileV.value 
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump.bump_map.coords.U_Tiling = DDNA_TileU.value
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump.bump_map.coords.V_Tiling = DDNA_TileV.value
			)

				if LayerSelect.selected=="Layer4" do
			(
				meditMaterials[activeMeditSlot].coatMtl[3].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling=DDNA_TileU.value
				meditMaterials[activeMeditSlot].coatMtl[3].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling=DDNA_TileV.value
				meditMaterials[activeMeditSlot].coatMtl[3].texmap_bump.bump_map.coords.U_Tiling = DDNA_TileU.value
				meditMaterials[activeMeditSlot].coatMtl[3].texmap_bump.bump_map.coords.V_Tiling = DDNA_TileV.value
			)

				if LayerSelect.selected=="Wear1" do
			(
				meditMaterials[activeMeditSlot].coatMtl[4].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling=DDNA_TileU.value
				meditMaterials[activeMeditSlot].coatMtl[4].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling=DDNA_TileV.value 
				meditMaterials[activeMeditSlot].coatMtl[4].texmap_bump.bump_map.coords.U_Tiling = DDNA_TileU.value
				meditMaterials[activeMeditSlot].coatMtl[4].texmap_bump.bump_map.coords.V_Tiling = DDNA_TileV.value
			)

				if LayerSelect.selected=="Wear2" do
			(
				
				meditMaterials[activeMeditSlot].coatMtl[5].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling=DDNA_TileU.value
				meditMaterials[activeMeditSlot].coatMtl[5].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling=DDNA_TileV.value
				meditMaterials[activeMeditSlot].coatMtl[5].texmap_bump.bump_map.coords.U_Tiling = DDNA_TileU.value
				meditMaterials[activeMeditSlot].coatMtl[5].texmap_bump.bump_map.coords.V_Tiling = DDNA_TileV.value
			)
				if LayerSelect.selected=="Wear3" do
			(
				meditMaterials[activeMeditSlot].coatMtl[6].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling=DDNA_TileU.value
				meditMaterials[activeMeditSlot].coatMtl[6].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling=DDNA_TileV.value 
				meditMaterials[activeMeditSlot].coatMtl[6].texmap_bump.bump_map.coords.U_Tiling = DDNA_TileU.value
				meditMaterials[activeMeditSlot].coatMtl[6].texmap_bump.bump_map.coords.V_Tiling = DDNA_TileV.value
			)
			if LayerSelect.selected=="Wear4" do
			(
			meditMaterials[activeMeditSlot].coatMtl[7].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling=DDNA_TileU.value
			meditMaterials[activeMeditSlot].coatMtl[7].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling=DDNA_TileV.value
			meditMaterials[activeMeditSlot].coatMtl[7].texmap_bump.bump_map.coords.U_Tiling = DDNA_TileU.value
			meditMaterials[activeMeditSlot].coatMtl[7].texmap_bump.bump_map.coords.V_Tiling = DDNA_TileV.value
			)	
					
					
)


)
createdialog LayerAdjuster
