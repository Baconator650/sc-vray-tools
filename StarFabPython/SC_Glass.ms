try (closeRolloutFloater SC_Glass) catch()

maxINI = getMAXIniFile()
tex_path = getINISetting maxINI "SCTools" "tex_path"

rollout SC_Glass "SC Glass Material Importer alpha 0.0.2" width:884 height:463
(
	
	bitmap 'bmp7' "Bitmap" pos:[61,10] width:768 height:80 fileName:"glassloader.tiff" align:#left
	bitmap 'bmp8' "Bitmap" pos:[795,204] width:82 height:82 align:#left fileName:"Logo_Community_logo.tiff"
	groupBox 'grp17' "Name and Path" pos:[8,98] width:293 height:67 align:#left
	groupBox 'grp18' "Diff [1.0]" pos:[305,100] width:104 height:77 align:#left
	groupBox 'grp19' "Spec [1.0]" pos:[412,99] width:94 height:77 align:#left
	groupBox 'grp20' "Gloss [255]" pos:[505,99] width:69 height:77 align:#left
	groupBox 'grp21' "Textures" pos:[8,179] width:674 height:121 align:#left
	groupBox 'grp22' "Dirt Color Parameters" pos:[287,319] width:152 height:140 align:#left
	groupBox 'grp23' "Blend Strength" pos:[785,100] width:94 height:91 align:#left
	listBox 'lbx_Thickness' "Thickness" pos:[15,306] width:103 height:6 items:#("Thin", "Medium", "Thick") align:#left
	spinner 'lbx_GlassType' "IOR" pos:[123,350] width:100 height:10 range:[0,20,1.459] align:#left
	groupBox 'grp24' "Refrac Color [255]" pos:[578,100] width:102 height:77 align:#left
	editText 'MatName' "Name" pos:[56,140] width:234 height:22 align:#left
	--NameandPath
	editText 'TexPath' "Texture Path" pos:[13,429] width:270 height:22 align:#left text: tex_path --SET THIS TO YOUR TEXTURE PATH
	--RGBssss
	spinner 'Diff_R' "R" pos:[318,118] width:78 height:16 range:[0,1,1] type:#float scale:1e-13  align:#left
	spinner 'Diff_G' "G" pos:[318,137] width:78 height:16 range:[0,1,1] type:#float scale:1e-13  align:#left
	spinner 'Diff_B' "B" pos:[318,157] width:79 height:16 range:[0,1,1] type:#float scale:1e-13  align:#left
	spinner 'Spec_R' "R" pos:[418,116] width:78 height:16 range:[0,1,1] type:#float scale:1e-13  align:#left
	spinner 'Spec_G' "G" pos:[418,135] width:78 height:16 range:[0,1,1] type:#float scale:1e-13  align:#left
	spinner 'Spec_B' "B" pos:[418,155] width:79 height:16 range:[0,1,1] type:#float scale:1e-13  align:#left
	spinner 'Dirt_R' "R" pos:[317,356] width:78 height:16 range:[0,1,1] type:#float scale:1e-13  align:#left
	spinner 'Dirt_G' "G" pos:[317,375] width:78 height:16 range:[0,1,1] type:#float scale:1e-13  align:#left
	spinner 'Dirt_B' "B" pos:[317,395] width:79 height:16 range:[0,1,1] type:#float scale:1e-13  align:#left
	spinner 'GlossMulti' "" pos:[512,135] width:61 height:16 range:[0,255,255] type:#float scale:1  align:#left
	spinner 'Refact_R' "R" pos:[582,118] width:78 height:16 range:[0,1,1] type:#float scale:1e-13  align:#left
	spinner 'Refact_G' "G" pos:[582,137] width:78 height:16 range:[0,1,1] type:#float scale:1e-13  align:#left
	spinner 'Refact_B' "B" pos:[582,157] width:79 height:16 range:[0,1,1] type:#float scale:1e-13  align:#left
	--
	groupBox 'grp25' "Refrac Gloss [1.0]" pos:[683,100] width:99 height:89 align:#left
	spinner 'Refract_Gloss' "" pos:[703,137] width:61 height:16 range:[0,1,1] type:#float scale:1e-13  align:#left
	label 'lbl5' "color" pos:[348,337] width:28 height:15 align:#left
	spinner 'Dirt_Reflect' "Reflectance" pos:[292,421] width:79 height:16 range:[0,1,0] scale:1e-13  align:#left
	spinner 'Dirt_Gloss' "Smoothness" pos:[292,440] width:79 height:16 range:[0,1,0] scale:1e-13  align:#left
	spinner 'Wear_blend' "" pos:[796,127] width:70 height:16 range:[0,100,0] scale:0.0001 align:#left
	spinner 'Dirt_Blend' "" pos:[796,167] width:69 height:16 range:[0,100,0] scale:0.0001 align:#left
	label 'lbl6' "wear" pos:[813,112] width:26 height:16 align:#left
	label 'lbl7' "Dirt" pos:[816,151] width:21 height:16 align:#left
	spinner 'RefractDist' "Refract Distance" pos:[450,368] width:79 height:16 range:[0,100,20] align:#left
	groupBox 'grp26' "Additional Parameters" pos:[441,320] width:170 height:138 align:#left
	editText 'TexMap_Diff' "Diff [TexSlot1]" pos:[35,193] width:525 height:22 align:#left
	editText 'TexMap_DDNA' "DDNA [TexSlot 2]" pos:[18,218] width:543 height:22 align:#left
	checkbox 'chk_gloss' "apply to gloss" pos:[566,223] width:87 height:15 align:#left
	
	groupBox 'Tint' "Tint" pos:[686,195] width:104 height:153 align:#left
	spinner 'Tint_R' "R" pos:[699,213] width:78 height:16 range:[0,255,1] type:#float scale:1e-13  align:#left
	spinner 'Tint_G' "G" pos:[699,232] width:78 height:16 range:[0,255,1] type:#float scale:1e-13  align:#left
	spinner 'Tint_B' "B" pos:[699,252] width:79 height:16 range:[0,255,1] type:#float scale:1e-13  align:#left
	checkbox 'chk_255' "255" pos:[725,269] width:43 height:28 align:#left
	editText 'Dirt_Wear_mask' "Dirt/Wear[TexSlot 11]" pos:[19,277] width:543 height:22 align:#left
	spinner 'Reflec_Dist' "Reflection Distance" pos:[448,349] width:66 height:16 range:[0,100,8] align:#left
	button 'btn_make' "Import" pos:[650,364] width:200 height:25 align:#left
	button 'btn_glad' "Import Gladius Glass" pos:[650,400] width:200 height:25 align:#left
	
	editText 'TexMap_GlossAlt' "Gloss [TexSlot 6]" pos:[19,246] width:543 height:22 align:#left
	editText 'MTL_name' "MTL Name" pos:[31,116] width:259 height:22 align:#left
	spinner 'TintPower' "TintCloud" pos:[732,302] width:49 height:16 range:[0,10,.1] type:#float scale:1e-13  align:#top
	spinner 'NormalTile' "BumpMapTile" pos:[565,252] width:50 height:22 align:#left range:[0,200,1]
	spinner 'bumpscale' "Bump Scale"  pos:[565,282] width:50 height:22 align:#left range:[0,1,1]
	button 'GML_Transfer' "Transfer from GML" pos:[140,390] width:100 height:20 align:#left
	checkbox 'CK_TP' "Tint Pallette" pos:[706,323] width:80 height:15 align:#left


	on GML_Transfer pressed do
	(
		mtl_in_filet = openFile "T://SC_Cache//BaseOutput.txt" mode: "rt" --SET THIS TO TEMP LOCATION
		
		mtlname=execute(readline mtl_in_filet)
		Base_diff=execute(readline mtl_in_filet) 
		Base_ddna=execute(readline mtl_in_filet) 
		Base_detail=execute(readline mtl_in_filet)
		Base_spec=execute(readline mtl_in_filet)
		BaseB_diff=execute(readline mtl_in_filet)
		Base_disp=execute(readline mtl_in_filet)
		BaseB_ddna=execute(readline mtl_in_filet)
		BaseB_detail=execute(readline mtl_in_filet)
		BaseB_spec=execute(readline mtl_in_filet)
		Base_mask=execute(readline mtl_in_filet)
		Base_SpecR=execute(readline mtl_in_filet)
		Base_SpecG=execute(readline mtl_in_filet)
		Base_SpecB=execute(readline mtl_in_filet)
		BaseB_SpecR=execute(readline mtl_in_filet)
		BaseB_SpecG=execute(readline mtl_in_filet)
		BaseB_SpecB=execute(readline mtl_in_filet)
		Base_DiffR=execute(readline mtl_in_filet)
		Base_DiffG=execute(readline mtl_in_filet)
		Base_DiffB=execute(readline mtl_in_filet)
		Base2_DiffR=execute(readline mtl_in_filet)
		Base2_DiffG=execute(readline mtl_in_filet)
		Base2_DiffB=execute(readline mtl_in_filet)
		Base_BlendLayerTile=execute(readline mtl_in_filet)
		Base_WearBlendBase=execute(readline mtl_in_filet)
		Base_BlendFactor=execute(readline mtl_in_filet)
		Base_Gloss=execute(readline mtl_in_filet)
		Base2_Gloss=execute(readline mtl_in_filet)
		Base_Tile=execute(readline mtl_in_filet)
		Base_Detail_DiffScale=execute(readline mtl_in_filet)
		Base2_Detail_DiffScale=execute(readline mtl_in_filet)
		Base_Detail_Nrm=execute(readline mtl_in_filet)
		BaseB_Detail_Nrm=execute(readline mtl_in_filet)
		Base_Detail_Gloss=execute(readline mtl_in_filet)
		BaseB_Detail_Gloss=execute(readline mtl_in_filet)
		Base_TintR=execute(readline mtl_in_filet)
		Base_TintG=execute(readline mtl_in_filet)
		Base_TintB=execute(readline mtl_in_filet)
		Base2_TintR=execute(readline mtl_in_filet)
		Base2_TintG=execute(readline mtl_in_filet)
		Base2_TintB=execute(readline mtl_in_filet)
		Base_LayerGloss=execute(readline mtl_in_filet)
		Base2_LayerGloss=execute(readline mtl_in_filet)
		Base_Layer1UV=execute(readline mtl_in_filet)
		Base2_Layer2UV=execute(readline mtl_in_filet)
		LayerTF2=execute(readline mtl_in_filet)
		LayerTF1=execute(readline mtl_in_filet)
		DisplOutput=execute(readline mtl_in_filet)
		TintPallete_Base=execute(readline mtl_in_filet)
		TintPallete_Blend=execute(readline mtl_in_filet)
		Base_Detail_TileU=execute(readline mtl_in_filet)
		Base_Detail_TileV=execute(readline mtl_in_filet)
		Blend_Detail_TileU=execute(readline mtl_in_filet)
		Blend_Detail_TileV=execute(readline mtl_in_filet)
		IllumR=execute(readline mtl_in_filet)
		IllumG=execute(readline mtl_in_filet)
		IllumB=execute(readline mtl_in_filet)
		Glow=execute(readline mtl_in_filet)
		Glass_R=execute(readline mtl_in_filet)
		Glass_G=execute(readline mtl_in_filet)
		Glass_B=execute(readline mtl_in_filet)
		TintCloudiness=execute(readline mtl_in_filet)
		
		if Glass_R  !="" do
		(
		Tint_R.value=Glass_R as float
		)
		
		if Glass_G  !="" do
		(
		Tint_G.value=Glass_G as float
		)
		
		if Glass_B  !="" do
		(
		Tint_B.value=Glass_B as float
		)
		
		if TintCloudiness  !="" do
		(
		TintPower.value=TintCloudiness as float
		)

		if mtlname  !="" do
		(
		MatName.text=mtlname as string
		)
		
		if Base_DiffR  !="" do
		(
		Diff_R.value=Base_DiffR as float
		)
		
		if Base_DiffG  !="" do
		(
		Diff_G.value=Base_DiffG as float
		)
		
		if Base_DiffB  !="" do
		(
		Diff_B.value=Base_DiffB as float
		)
		
		if Base_Diff  !="" do
		(
		TexMap_Diff.text=Base_diff as string
		)
		
		if Base_ddna  !="" do
		(
		TexMap_ddna.text=Base_ddna as string
		)
		
		
		if Base_Gloss  !="" do
		(
		GlossMulti.value=Base_Gloss as float
		)
		
		if Base_SpecR  !="" do
		(
		Spec_R.value=Base_SpecR as float
		)
		
		if Base_SpecG  !="" do
		(
		Spec_G.value=Base_SpecG as float
		)
		
		if Base_SpecB  !="" do
		(
		Spec_B.value=Base_SpecB as float
		)
	
		close mtl_in_filet
	)
	
	on btn_make pressed do
	(
		if MatName.text !="" do
		(
			meditMaterials[activeMeditSlot]=VRayBlendMtl()
			meditMaterials[activeMeditSlot].baseMtl=VRayMtl()
			meditMaterials[activeMeditSlot].baseMtl.diffuse= color (Diff_R.value*255) (Diff_G.value*255) (Diff_B.value*255)
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse= CompositeTexturemap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].color=color (Diff_R.value*255) (Diff_G.value*255) (Diff_B.value*255) 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.blendMode[2]=5
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].name="Glass FOG Color"
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection= CompositeTexturemap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[2]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[2].Color= color (Spec_R.value*255) (Spec_G.value*255) (Spec_B.value*255)
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.blendMode[2]=0
			meditMaterials[activeMeditSlot].baseMtl.reflection= color (Spec_R.value*255) (Spec_G.value*255) (Spec_B.value*255)
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[2].lightnessMode = 1
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[2].gammaRGB = 2.2
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness= CompositeTexturemap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1]=CompositeTextureMap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.opacity[1]=((GlossMulti.value/255)*100)
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump=VRayNormalMap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump_multiplier =100
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump_multiplier=(bumpscale.value*100)
			meditMaterials[activeMeditSlot].name=(MTL_name.text+"_mtl_"+MatName.text)
			
			meditMaterials[activeMeditSlot].coatMtl[1]=VRayMtl()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse= CompositeTexturemap()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[2]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[2].color=color (Diff_R.value*255) (Diff_G.value*255) (Diff_B.value*255) 255
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.blendMode[2]=5
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection= CompositeTexturemap()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[2]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[2].Color= color (Spec_R.value*255) (Spec_G.value*255) (Spec_B.value*255)
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.blendMode[2]=5
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness= CompositeTexturemap()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[2]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[2].color= color (Dirt_Gloss.value*255) (Dirt_Gloss.value*255) (Dirt_Gloss.value*255) 255
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.blendMode[2]=5
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump=VRayNormalMap()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump_multiplier =100
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump_multiplier=(bumpscale.value*100)
			
			meditMaterials[activeMeditSlot].coatMtl[2]=VRayMtl()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse= CompositeTexturemap()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[2]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[2].color=color (Dirt_R.value*255) (Dirt_G.value*255) (Dirt_B.value*255) 255
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.blendMode[2]=5
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflection= CompositeTexturemap()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflection.maplist[2]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflection.maplist[2].Color= color (Dirt_Reflect.value*255) (Dirt_Reflect.value*255) (Dirt_Reflect.value*255)
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflection.blendMode[2]=5
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflectionGlossiness= CompositeTexturemap()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflectionGlossiness.opacity[1]=((Dirt_Gloss.value/255)*100)
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump=VRayNormalMap()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump_multiplier =100
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump_multiplier =(bumpscale.value*100)
			
			meditMaterials[activeMeditSlot].texmap_blend[1] = CompositeTexturemap ()
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2]=colorcorrection()
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2].color=color (Wear_blend.value*255) (Wear_blend.value*255) (Wear_blend.value*255) 255
			meditMaterials[activeMeditSlot].texmap_blend[1].blendMode[2]=5
			
			
			meditMaterials[activeMeditSlot].texmap_blend[2] = CompositeTexturemap ()
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1]=CompositeTexturemap()
			meditMaterials[activeMeditSlot].texmap_blend[2].opacity[1]=(Dirt_Blend.value*100)
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].maplist[2]=VRayDirt()
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].maplist[2]=VRayDirt()
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].maplist[2].radius=10000
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].maplist[2].occluded_color = color 255 255 255
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].maplist[2].unoccluded_color= color 0 0 0
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].maplist[2].affect_alpha = on
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].maplist[2].mode = 0
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].maplist[2].Distribution=.1
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].maplist[2].falloff=0.7
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].maplist[2].subdivs=12
			
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].maplist[1]=Noise()
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].opacity[1]=20
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].maplist[1].size=1.6
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].maplist[1].thresholdHigh=.58
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].maplist[1].thresholdLow=0.0
			
			meditMaterials[activeMeditSlot].baseMtl.refraction_fogDepth=(TintPower.value/100)
			meditMaterials[activeMeditSlot].coatMtl[1].refraction_fogDepth=(TintPower.value/100)
			
			
			meditMaterials[activeMeditSlot].baseMtl.refraction=color (Refact_R.value*255)(Refact_G.value*255)(Refact_B.value*255)
			meditMaterials[activeMeditSlot].coatMtl[1].refraction=color (Refact_R.value*255)(Refact_G.value*255)(Refact_B.value*255)
			meditMaterials[activeMeditSlot].coatMtl[2].refraction=color (Refact_R.value*255)(Refact_G.value*255)(Refact_B.value*255)
			
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_refraction= CompositeTexturemap()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_refraction.maplist[2]=colorcorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_refraction.maplist[2].color=color (Refact_R.value*255)(Refact_G.value*255)(Refact_B.value*255)
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_refraction.blendMode[2]=5
			
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_refraction= CompositeTexturemap()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_refraction.maplist[2]=colorcorrection()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_refraction.maplist[2].color=color (Refact_R.value*255)(Refact_G.value*255)(Refact_B.value*255)
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_refraction.blendMode[2]=5
			
			meditMaterials[activeMeditSlot].baseMtl.texmap_refraction= CompositeTexturemap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_refraction.maplist[2]=colorcorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_refraction.maplist[2].color=color (Refact_R.value*255)(Refact_G.value*255)(Refact_B.value*255)
			meditMaterials[activeMeditSlot].baseMtl.texmap_refraction.blendMode[2]=5
			
			meditMaterials[activeMeditSlot].baseMtl.refraction_glossiness=Refract_Gloss.value
			meditMaterials[activeMeditSlot].coatMtl[1].refraction_glossiness=Refract_Gloss.value
			meditMaterials[activeMeditSlot].coatMtl[2].refraction_glossiness=Refract_Gloss.value
			meditMaterials[activeMeditSlot].baseMtl.refraction_fogMult=0.001
			
			meditMaterials[activeMeditSlot].baseMtl.refraction_maxDepth=RefractDist.value
			meditMaterials[activeMeditSlot].coatMtl[1].refraction_maxDepth=RefractDist.value
			meditMaterials[activeMeditSlot].coatMtl[2].refraction_maxDepth=RefractDist.value
			
			meditMaterials[activeMeditSlot].baseMtl.reflection_maxDepth=Reflec_Dist.value
			meditMaterials[activeMeditSlot].coatMtl[1].reflection_maxDepth=Reflec_Dist.value
			meditMaterials[activeMeditSlot].coatMtl[2].reflection_maxDepth=Reflec_Dist.value

		)

			if CK_TP.state==true do
			(
			meditMaterials[activeMeditSlot].baseMtl.texmap_refraction_fog= Color_Correction ()
			meditMaterials[activeMeditSlot].baseMtl.texmap_refraction_fog.name="Glass FOG Color"
			meditMaterials[activeMeditSlot].baseMtl.texmap_refraction_fog.color=color (Tint_R.value*255) (Tint_G.value*255) (Tint_B.value*255) 
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_refraction_fog=meditMaterials[activeMeditSlot].baseMtl.texmap_refraction_fog
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_refraction_fog=meditMaterials[activeMeditSlot].baseMtl.texmap_refraction_fog
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].name="Glass FOG Color"
			)	
					
			if chk_255.state==true then
			(
				meditMaterials[activeMeditSlot].baseMtl.refraction_fogColor=color Tint_R.value Tint_G.value Tint_B.value
				meditMaterials[activeMeditSlot].coatMtl[1].refraction_fogColor=color Tint_R.value Tint_G.value Tint_B.value
				meditMaterials[activeMeditSlot].coatMtl[2].refraction_fogColor=color Tint_R.value Tint_G.value Tint_B.value
			)
			else
			(
				meditMaterials[activeMeditSlot].baseMtl.refraction_fogColor=color (Tint_R.value*255) (Tint_G.value*255) (Tint_B.value*255) 
				meditMaterials[activeMeditSlot].coatMtl[1].refraction_fogColor=color (Tint_R.value*255) (Tint_G.value*255) (Tint_B.value*255)
				meditMaterials[activeMeditSlot].coatMtl[2].refraction_fogColor=color (Tint_R.value*255) (Tint_G.value*255) (Tint_B.value*255)
			)
			if TexMap_Diff.text!="" do
			(
				meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1]=VRayBitmap()
				meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1].HDRIMapName=(TexPath.text+TexMap_Diff.text)
				meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1].alphaSource=2
				meditMaterials[activeMeditSlot].baseMtl.texmap_refraction.maplist[1]=VRayBitmap()
				meditMaterials[activeMeditSlot].baseMtl.texmap_refraction.maplist[1].HDRIMapName=(TexPath.text+TexMap_Diff.text)
				meditMaterials[activeMeditSlot].baseMtl.texmap_refraction.maplist[1].color_space=0
				meditMaterials[activeMeditSlot].baseMtl.texmap_refraction.maplist[1].alphaSource=0
				meditMaterials[activeMeditSlot].baseMtl.texmap_refraction.maplist[1].rgbOutput = 1
				meditMaterials[activeMeditSlot].baseMtl.texmap_refraction.maplist[1].monoOutput = 1
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_refraction.maplist[1]=VRayBitmap()
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_refraction.maplist[1].HDRIMapName=(TexPath.text+TexMap_Diff.text)
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_refraction.maplist[1].color_space=0
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_refraction.maplist[1].alphaSource=0
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_refraction.maplist[1].rgbOutput = 1
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_refraction.maplist[1].monoOutput = 1
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_refraction.maplist[1]=VRayBitmap()
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_refraction.maplist[1].HDRIMapName=(TexPath.text+TexMap_Diff.text)
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_refraction.maplist[1].color_space=0
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_refraction.maplist[1].alphaSource=0
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_refraction.maplist[1].rgbOutput = 1
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_refraction.maplist[1].monoOutput = 1
			)
			
			if TexMap_DDNA.text!="" and chk_gloss.state==true do
			(			
				SCNormalString = TexMap_DDNA.text
				GlossPath=substring SCNormalString 1 (SCNormalString.count-4)+"_glossMap.tif"
				meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map=VRayBitmap()
				meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map.HDRIMapName=(TexPath.text+TexMap_DDNA.text)
				meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map.color_space=0
				meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map.coords.V_Offset=NormalTile.value
				meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map.coords.U_Offset=NormalTile.value
				
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.normal_map=VRayBitmap()
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.normal_map.HDRIMapName=(TexPath.text+TexMap_DDNA.text)
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.normal_map.color_space=0
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.normal_map.coords.V_Offset=NormalTile.value
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.normal_map.coords.U_Offset=NormalTile.value
				
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump.normal_map=VRayBitmap()
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump.normal_map.HDRIMapName=(TexPath.text+TexMap_DDNA.text)
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump.normal_map.color_space=0
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump.normal_map.coords.V_Offset=NormalTile.value
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump.normal_map.coords.U_Offset=NormalTile.value
				
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1]=VRayBitmap()
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].HDRIMapName=(TexPath.text+GlossPath)
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].color_space=0
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Offset=NormalTile.value
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Offset=NormalTile.value
				
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflectionGlossiness.maplist[1]=VRayBitmap()
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflectionGlossiness.maplist[1].HDRIMapName=(TexPath.text+GlossPath)
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflectionGlossiness.maplist[1].color_space=0
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflectionGlossiness.maplist[1].coords.V_Offset=NormalTile.value
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflectionGlossiness.maplist[1].coords.U_Offset=NormalTile.value
				
			)
			
			if TexMap_DDNA.text!="" and chk_gloss.state==false do
			(
				meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map=VRayBitmap()
				meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map.HDRIMapName=(TexPath.text+TexMap_DDNA.text)
				meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map.color_space=0
				meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map.coords.V_Offset=NormalTile.value
				meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map.coords.U_Offset=NormalTile.value
				
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.normal_map=VRayBitmap()
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.normal_map.HDRIMapName=(TexPath.text+TexMap_DDNA.text)
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.normal_map.color_space=0
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.normal_map.coords.V_Offset=NormalTile.value
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.normal_map.coords.U_Offset=NormalTile.value
				
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump.normal_map=VRayBitmap()
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump.normal_map.HDRIMapName=(TexPath.text+TexMap_DDNA.text)
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump.normal_map.color_space=0
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump.normal_map.coords.V_Offset=NormalTile.value
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump.normal_map.coords.U_Offset=NormalTile.value
			)
			
			if TexMap_DDNA.text=="" do
			(
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1]=colorcorrection()
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].color= color 255 255 255
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflectionGlossiness.maplist[1]=colorcorrection()
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_reflectionGlossiness.maplist[1].color= color 255 255 255
			)

			if TexMap_GlossAlt.text!="" and TexMap_DDNA.text=="" do
			(
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1]=VRayBitmap()
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].HDRIMapName=(TexPath.text+TexMap_DDNA.text)
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].color_space=0
				
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[1]=VRayBitmap()
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[1].HDRIMapName=(TexPath.text+TexMap_DDNA.text)
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[1].color_space=0
			)
			if Dirt_Wear_mask.text!="" do
			(
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[1]=VRayBitmap()
				meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[1].HDRIMapName=(TexPath.text+Dirt_Wear_mask.text)
			
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[1]=VRayBitmap()
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[1].HDRIMapName=(TexPath.text+Dirt_Wear_mask.text)
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[1].color_space=0
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[1].alphaSource=0
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[1].rgbOutput = 1
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[1].monoOutput = 1
				
				
			)
			
			if lbx_Thickness.selected=="Thin" do 
			(
				
				meditMaterials[activeMeditSlot].baseMtl.reflection_lockIOR = off
				meditMaterials[activeMeditSlot].coatMtl[1].reflection_lockIOR = off
				meditMaterials[activeMeditSlot].coatMtl[2].reflection_lockIOR = off
				
				meditMaterials[activeMeditSlot].baseMtl.refraction_ior=1.0
				meditMaterials[activeMeditSlot].coatMtl[1].refraction_ior=1.0
				meditMaterials[activeMeditSlot].coatMtl[2].refraction_ior=1.0
				
				meditMaterials[activeMeditSlot].baseMtl.reflection_IOR=lbx_GlassType.value
				meditMaterials[activeMeditSlot].coatMtl[1].reflection_IOR=lbx_GlassType.value
				meditMaterials[activeMeditSlot].coatMtl[2].reflection_IOR=lbx_GlassType.value

				meditMaterials[activeMeditSlot].baseMtl.refraction_thinWalled = on
				meditMaterials[activeMeditSlot].coatMtl[1].refraction_thinWalled = on
			)
			
			if lbx_Thickness.selected=="Medium" do 
			(
				
				meditMaterials[activeMeditSlot].baseMtl.reflection_lockIOR = off
				meditMaterials[activeMeditSlot].coatMtl[1].reflection_lockIOR = off
				meditMaterials[activeMeditSlot].coatMtl[2].reflection_lockIOR = off
				
				meditMaterials[activeMeditSlot].baseMtl.refraction_ior=1.2
				meditMaterials[activeMeditSlot].coatMtl[1].refraction_ior=1.2
				meditMaterials[activeMeditSlot].coatMtl[2].refraction_ior=1.2
				
				meditMaterials[activeMeditSlot].baseMtl.reflection_IOR=lbx_GlassType.value
				meditMaterials[activeMeditSlot].coatMtl[1].reflection_IOR=lbx_GlassType.value
				meditMaterials[activeMeditSlot].coatMtl[2].reflection_IOR=lbx_GlassType.value
			)
			
			if lbx_Thickness.selected=="Thick" do 
			(
				meditMaterials[activeMeditSlot].baseMtl.refraction_ior=lbx_GlassType.value
				meditMaterials[activeMeditSlot].coatMtl[1].refraction_ior=lbx_GlassType.value
				meditMaterials[activeMeditSlot].coatMtl[2].refraction_ior=lbx_GlassType.value
			)
			
			if TexMap_DDNA.text=="" do
			( if chk_gloss.state==true do
			(
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1]=VRayBitmap()
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].HDRIMapName=(TexPath.text+TexMap_DDNA.text)
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].color_space=0
				
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[1]=VRayBitmap()
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[1].HDRIMapName=(TexPath.text+TexMap_DDNA.text)
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[1].color_space=0
			)
			)
			



		)
)

CreateDialog SC_Glass
