try (closeRolloutFloater SC_DirtRenamer) catch()

rollout SC_DirtRenamer "Dirt Renaming Tool" width:200 height:100
(

	button 'EXT_name' "Exterior Dirt" pos:[40,20] width: 130 height: 20 align:#left
	button 'INT_name' "Interior Dirt" pos:[40,60] width: 130 height: 20 align:#left	
	
	on EXT_name pressed do
	(
		if iskindof meditMaterials[activeMeditSlot] VRayMtl do 
		(
			try 		
			(
				meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_EXT"
				meditMaterials[activeMeditSlot].texmap_diffuse.mask[6].name = "DIRT_INNER_MASK_EXT"
				
			)
			catch()
		)
		if iskindof meditMaterials[activeMeditSlot] VRayBlendMtl do
		(
			try 		
			(
				meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_EXT"
				meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6].name = "DIRT_INNER_MASK_EXT"
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_EXT"
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[6].name = "DIRT_INNER_MASK_EXT"
				
				if iskindof meditMaterials[activeMeditSlot].texmap_blend[1] Color_Correction do
				(
					meditMaterials[activeMeditSlot].texmap_blend[1].name =(meditMaterials[activeMeditSlot].texmap_blend[1].name) + "_EXT"
				)
				
				if iskindof meditMaterials[activeMeditSlot].texmap_blend[1] CompositeMap do
				(
					meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2].name = (meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2].name) + "_EXT"
				)
			)
			catch()
		)
	)
	
	on INT_name pressed do
	(
		if iskindof meditMaterials[activeMeditSlot] VRayMTL do
		(
			try 		
			(
				meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_INT"
				meditMaterials[activeMeditSlot].texmap_diffuse.mask[6].name = "DIRT_INNER_MASK_INT"
				
			)
			catch()
		)
		if iskindof meditMaterials[activeMeditSlot] VRayBlendMtl do
		(
			try 		
			(
				meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_INT"
				meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6].name = "DIRT_INNER_MASK_INT"
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_INT"
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[6].name = "DIRT_INNER_MASK_INT"
				
				if iskindof meditMaterials[activeMeditSlot].texmap_blend[1] Color_Correction do
				(
					meditMaterials[activeMeditSlot].texmap_blend[1].name =(meditMaterials[activeMeditSlot].texmap_blend[1].name) + "_INT"
				)
				
				if iskindof meditMaterials[activeMeditSlot].texmap_blend[1] CompositeMap do
				(
					meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2].name = (meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2].name) + "_INT"
				)
			)
			catch()
			
				
		)
	)
)

CreateDialog SC_DirtRenamer