try (closeRolloutFloater SC_Mat_Aid) catch()




rollout SC_MAT_CONTROLLER_CUBE "Wear/Dirt Material Control Cube" width:700 height:480
(
	label 'lbl4' "THIS CREATES THE ALMIGHT MATERIAL CONTROL CUBE. USE THIS MIGHTY CUBE TO CREATE A NON RENDERABLE CUBE THAT CONTAINS SEVERAL MATERIALS THAT CAN BE INSTANCE WITH KSTUDIO'S INSTANCE MATERIAL BY NAME SCRIPT. THEN USE THAT CUBE TO CONTROL YOUR SHIP/SCENE/OBJECT's Wear and DIRT CONTROL" pos:[130,20] width:400 height:120 align:#left
	button 'CubeCreate' "Create Wear/Dirt Material Control Cube " pos:[255,380] width:220 height:40 align:#left
	editText 'Suff1' "Material Suffix 1" pos:[170,110] width:303 height:20 align:#left text:"EXT"
	editText 'Suff2' "Material Suffix 1" pos:[170,130] width:303 height:20 align:#left text:"INT"	
	editText 'Suff3' "Material Suffix 1" pos:[170,150] width:303 height:20 align:#left text:"CARGO"	
	editText 'Suff4' "Material Suffix 1" pos:[170,170] width:303 height:20 align:#left text:"MISSILE"
	editText 'ShipName' "Ship Name" pos:[170,200] width:303 height:20 align:#left 
	
	on CubeCreate pressed do
	(
		ControlBox=Box pos:[-100,-100,0] isSelected:on width:25 length:25 height:25 name: (ShipName.text +" MASTER CONTROL CUBE")
		
		ControlBox.renderable=off
		
		ControlBox.mat= VRayMTL()
		ControlBox.mat.name=ShipName.text +" WEAR DIRT CONTROL CUBE MATERIAL"
		---Suff1
		ControlBox.mat.texmap_diffuse=CompositeMap()
		ControlBox.mat.texmap_diffuse.name="Control"+Suff1.text
		ControlBox.mat.texmap_diffuse.maplist[1]=ColorCorrection()
		ControlBox.mat.texmap_diffuse.maplist[1].name="Blend_CONTROL_"+Suff1.text
		ControlBox.mat.texmap_diffuse.maplist[1].color=color 255 255 255 255
		
		ControlBox.mat.texmap_diffuse.maplist[2]=ColorCorrection()
		ControlBox.mat.texmap_diffuse.maplist[2].name="Wear_CONTROL_"+Suff1.text
		ControlBox.mat.texmap_diffuse.maplist[2].color=color 0 0 0 255
		
		ControlBox.mat.texmap_diffuse.maplist[4]=CompositeMap()
		ControlBox.mat.texmap_diffuse.maplist[4].maplist[1]=VRayDirt()
		ControlBox.mat.texmap_diffuse.maplist[4].maplist[1].radius=10000
		ControlBox.mat.texmap_diffuse.maplist[4].maplist[1].occluded_color = color 255 255 255
		ControlBox.mat.texmap_diffuse.maplist[4].maplist[1].unoccluded_color= color 0 0 0
		ControlBox.mat.texmap_diffuse.maplist[4].maplist[1].affect_alpha = on
		ControlBox.mat.texmap_diffuse.maplist[4].maplist[1].mode = 0
		ControlBox.mat.texmap_diffuse.maplist[4].maplist[1].Distribution=.1
		ControlBox.mat.texmap_diffuse.maplist[4].maplist[1].falloff=0.7
		ControlBox.mat.texmap_diffuse.maplist[4].maplist[1].subdivs=12
		ControlBox.mat.texmap_diffuse.maplist[4].opacity[1]=0.0
		ControlBox.mat.texmap_diffuse.maplist[4].name = "EDGE_DIRT_MASK_"+Suff1.text			
		
		ControlBox.mat.texmap_diffuse.maplist[3]=CompositeMap()
		ControlBox.mat.texmap_diffuse.maplist[3].maplist[1]=Noise()
		ControlBox.mat.texmap_diffuse.maplist[3].opacity[1]=0.0
		ControlBox.mat.texmap_diffuse.maplist[3].maplist[1].size=0.3
		ControlBox.mat.texmap_diffuse.maplist[3].maplist[1].thresholdHigh = 1
		ControlBox.mat.texmap_diffuse.maplist[3].maplist[1].color1 = color 255 255 255
		ControlBox.mat.texmap_diffuse.maplist[3].maplist[1].color2 = color 0 0 0

		ControlBox.mat.texmap_diffuse.maplist[3].name="DIRT_INNER_MASK_"+Suff1.text 
		
			---Suff2
		ControlBox.mat.texmap_roughness=CompositeMap()
		ControlBox.mat.texmap_roughness.name="Control"+Suff2.text
		ControlBox.mat.texmap_roughness.maplist[1]=ColorCorrection()
		ControlBox.mat.texmap_roughness.maplist[1].name="Blend_CONTROL_"+Suff2.text
		ControlBox.mat.texmap_roughness.maplist[1].color=color 255 255 255 255
		
		ControlBox.mat.texmap_roughness.maplist[2]=ColorCorrection()
		ControlBox.mat.texmap_roughness.maplist[2].name="Wear_CONTROL_"+Suff2.text
		ControlBox.mat.texmap_roughness.maplist[2].color=color 0 0 0 255
		
		ControlBox.mat.texmap_roughness.maplist[4]=CompositeMap()
		ControlBox.mat.texmap_roughness.maplist[4].maplist[1]=VRayDirt()
		ControlBox.mat.texmap_roughness.maplist[4].maplist[1].radius=10000
		ControlBox.mat.texmap_roughness.maplist[4].maplist[1].occluded_color = color 255 255 255
		ControlBox.mat.texmap_roughness.maplist[4].maplist[1].unoccluded_color= color 0 0 0
		ControlBox.mat.texmap_roughness.maplist[4].maplist[1].affect_alpha = on
		ControlBox.mat.texmap_roughness.maplist[4].maplist[1].mode = 0
		ControlBox.mat.texmap_roughness.maplist[4].maplist[1].Distribution=.1
		ControlBox.mat.texmap_roughness.maplist[4].maplist[1].falloff=0.7
		ControlBox.mat.texmap_roughness.maplist[4].maplist[1].subdivs=12
		ControlBox.mat.texmap_roughness.maplist[4].opacity[1]=0.0
		ControlBox.mat.texmap_roughness.maplist[4].name = "EDGE_DIRT_MASK_"+Suff2.text			
		
		
		
		
		ControlBox.mat.texmap_roughness.maplist[3]=CompositeMap()
		ControlBox.mat.texmap_roughness.maplist[3].maplist[1]=Noise()
		ControlBox.mat.texmap_roughness.maplist[3].opacity[1]=0.0
		ControlBox.mat.texmap_roughness.maplist[3].maplist[1].size=0.3
		ControlBox.mat.texmap_roughness.maplist[3].maplist[1].thresholdHigh = 1
		ControlBox.mat.texmap_roughness.maplist[3].maplist[1].color1 = color 255 255 255
		ControlBox.mat.texmap_roughness.maplist[3].maplist[1].color2 = color 0 0 0

		ControlBox.mat.texmap_roughness.maplist[3].name="DIRT_INNER_MASK_"+Suff2.text 
		
		---Suff3
		
		ControlBox.mat.texmap_reflection=CompositeMap()
		ControlBox.mat.texmap_reflection.name="Control"+Suff3.text
		ControlBox.mat.texmap_reflection.maplist[1]=ColorCorrection()
		ControlBox.mat.texmap_reflection.maplist[1].name="Blend_CONTROL_"+Suff3.text
		ControlBox.mat.texmap_reflection.maplist[1].color=color 255 255 255 255
		
		ControlBox.mat.texmap_reflection.maplist[2]=ColorCorrection()
		ControlBox.mat.texmap_reflection.maplist[2].name="Wear_CONTROL_"+Suff3.text
		ControlBox.mat.texmap_reflection.maplist[2].color=color 0 0 0 255
		
		ControlBox.mat.texmap_reflection.maplist[4]=CompositeMap()
		ControlBox.mat.texmap_reflection.maplist[4].maplist[1]=VRayDirt()
		ControlBox.mat.texmap_reflection.maplist[4].maplist[1].radius=10000
		ControlBox.mat.texmap_reflection.maplist[4].maplist[1].occluded_color = color 255 255 255
		ControlBox.mat.texmap_reflection.maplist[4].maplist[1].unoccluded_color= color 0 0 0
		ControlBox.mat.texmap_reflection.maplist[4].maplist[1].affect_alpha = on
		ControlBox.mat.texmap_reflection.maplist[4].maplist[1].mode = 0
		ControlBox.mat.texmap_reflection.maplist[4].maplist[1].Distribution=.1
		ControlBox.mat.texmap_reflection.maplist[4].maplist[1].falloff=0.7
		ControlBox.mat.texmap_reflection.maplist[4].maplist[1].subdivs=12
		ControlBox.mat.texmap_reflection.maplist[4].opacity[1]=0.0
		ControlBox.mat.texmap_reflection.maplist[4].name = "EDGE_DIRT_MASK_"+Suff3.text			
		
		
		
		
		ControlBox.mat.texmap_reflection.maplist[3]=CompositeMap()
		ControlBox.mat.texmap_reflection.maplist[3].maplist[1]=Noise()
		ControlBox.mat.texmap_reflection.maplist[3].opacity[1]=0.0
		ControlBox.mat.texmap_reflection.maplist[3].maplist[1].size=0.3
		ControlBox.mat.texmap_reflection.maplist[3].maplist[1].thresholdHigh = 1
		ControlBox.mat.texmap_reflection.maplist[3].maplist[1].color1 = color 255 255 255
		ControlBox.mat.texmap_reflection.maplist[3].maplist[1].color2 = color 0 0 0

		ControlBox.mat.texmap_reflection.maplist[3].name="DIRT_INNER_MASK_"+Suff3.text 
		
		---Suff4
		
		ControlBox.mat.texmap_reflectionGlossiness=CompositeMap()
		ControlBox.mat.texmap_reflectionGlossiness.name="Control"+Suff4.text
		ControlBox.mat.texmap_reflectionGlossiness.maplist[1]=ColorCorrection()
		ControlBox.mat.texmap_reflectionGlossiness.maplist[1].name="Blend_CONTROL_"+Suff4.text
		ControlBox.mat.texmap_reflectionGlossiness.maplist[1].color=color 255 255 255 255
		
		ControlBox.mat.texmap_reflectionGlossiness.maplist[2]=ColorCorrection()
		ControlBox.mat.texmap_reflectionGlossiness.maplist[2].name="Wear_CONTROL_"+Suff4.text
		ControlBox.mat.texmap_reflectionGlossiness.maplist[2].color=color 0 0 0 255
		
		ControlBox.mat.texmap_reflectionGlossiness.maplist[4]=CompositeMap()
		ControlBox.mat.texmap_reflectionGlossiness.maplist[4].maplist[1]=VRayDirt()
		ControlBox.mat.texmap_reflectionGlossiness.maplist[4].maplist[1].radius=10000
		ControlBox.mat.texmap_reflectionGlossiness.maplist[4].maplist[1].occluded_color = color 255 255 255
		ControlBox.mat.texmap_reflectionGlossiness.maplist[4].maplist[1].unoccluded_color= color 0 0 0
		ControlBox.mat.texmap_reflectionGlossiness.maplist[4].maplist[1].affect_alpha = on
		ControlBox.mat.texmap_reflectionGlossiness.maplist[4].maplist[1].mode = 0
		ControlBox.mat.texmap_reflectionGlossiness.maplist[4].maplist[1].Distribution=.1
		ControlBox.mat.texmap_reflectionGlossiness.maplist[4].maplist[1].falloff=0.7
		ControlBox.mat.texmap_reflectionGlossiness.maplist[4].maplist[1].subdivs=12
		ControlBox.mat.texmap_reflectionGlossiness.maplist[4].opacity[1]=0.0
		ControlBox.mat.texmap_reflectionGlossiness.maplist[4].name = "EDGE_DIRT_MASK_"+Suff4.text			
		
		
		
		
		ControlBox.mat.texmap_reflectionGlossiness.maplist[3]=CompositeMap()
		ControlBox.mat.texmap_reflectionGlossiness.maplist[3].maplist[1]=Noise()
		ControlBox.mat.texmap_reflectionGlossiness.maplist[3].opacity[1]=0.0
		ControlBox.mat.texmap_reflectionGlossiness.maplist[3].maplist[1].size=0.3
		ControlBox.mat.texmap_reflectionGlossiness.maplist[3].maplist[1].thresholdHigh = 1
		ControlBox.mat.texmap_reflectionGlossiness.maplist[3].maplist[1].color1 = color 255 255 255
		ControlBox.mat.texmap_reflectionGlossiness.maplist[3].maplist[1].color2 = color 0 0 0

		ControlBox.mat.texmap_reflectionGlossiness.maplist[3].name="DIRT_INNER_MASK_"+Suff4.text 
		---SuffNone
		
		ControlBox.mat.texmap_bump=CompositeMap()
		ControlBox.mat.texmap_bump.name="Control"
		ControlBox.mat.texmap_bump.maplist[1]=ColorCorrection()
		ControlBox.mat.texmap_bump.maplist[1].name="Blend_CONTROL"
		ControlBox.mat.texmap_bump.maplist[1].color=color 255 255 255 255
		
		ControlBox.mat.texmap_bump.maplist[2]=ColorCorrection()
		ControlBox.mat.texmap_bump.maplist[2].name="Wear_CONTROL"
		ControlBox.mat.texmap_bump.maplist[2].color=color 0 0 0 255
		
		ControlBox.mat.texmap_bump.maplist[4]=CompositeMap()
		ControlBox.mat.texmap_bump.maplist[4].maplist[1]=VRayDirt()
		ControlBox.mat.texmap_bump.maplist[4].maplist[1].radius=10000
		ControlBox.mat.texmap_bump.maplist[4].maplist[1].occluded_color = color 255 255 255
		ControlBox.mat.texmap_bump.maplist[4].maplist[1].unoccluded_color= color 0 0 0
		ControlBox.mat.texmap_bump.maplist[4].maplist[1].affect_alpha = on
		ControlBox.mat.texmap_bump.maplist[4].maplist[1].mode = 0
		ControlBox.mat.texmap_bump.maplist[4].maplist[1].Distribution=.1
		ControlBox.mat.texmap_bump.maplist[4].maplist[1].falloff=0.7
		ControlBox.mat.texmap_bump.maplist[4].maplist[1].subdivs=12
		ControlBox.mat.texmap_bump.maplist[4].opacity[1]=0.0
		ControlBox.mat.texmap_bump.maplist[4].name = "EDGE_DIRT_MASK"			
		
		
		
		
		ControlBox.mat.texmap_bump.maplist[3]=CompositeMap()
		ControlBox.mat.texmap_bump.maplist[3].maplist[1]=Noise()
		ControlBox.mat.texmap_bump.maplist[3].opacity[1]=0.0
		ControlBox.mat.texmap_bump.maplist[3].maplist[1].size=0.3
		ControlBox.mat.texmap_bump.maplist[3].maplist[1].thresholdHigh = 1
		ControlBox.mat.texmap_bump.maplist[3].maplist[1].color1 = color 255 255 255
		ControlBox.mat.texmap_bump.maplist[3].maplist[1].color2 = color 0 0 0

		ControlBox.mat.texmap_bump.maplist[3].name="DIRT_INNER_MASK" 
		
		
	)
	
)


createDialog SC_MAT_CONTROLLER_CUBE 