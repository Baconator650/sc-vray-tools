try (closeRolloutFloater SC_layerimporter) catch()

maxINI = getMAXIniFile()
tex_path = getINISetting maxINI "SCTools" "tex_path"


rollout SC_layerimporter "Layer Importer for WDA (VRay) alpha 0.0.2" width:768 height:376
(

	bitmap 'TitlePic' "Bitmap" pos:[197,7] width:380 height:72 fileName:"SCLayer.tif" align:#left
	editText 'Mat_Name' "Layer Name" pos:[24,100] width:328 height:20 align:#left
	editText 'Texture_SRC' "Texture Pathway" pos:[8,328] width:328 height:20 align:#left text:tex_path --SET THIS TO YOUR TEXTURE PATH
	groupBox 'grp1' "Diffuse (1.0)" pos:[22,139] width:287 height:50 align:#lef
	groupBox 'grp2' "Spec (1.0)" pos:[320,139] width:287 height:50 align:#left
	groupBox 'grp3' "Gloss (255)" pos:[614,139] width:122 height:50 align:#left
	spinner 'Diff_R' "R" pos:[32,160] width:80 height:16 range:[0,1,1] scale:1e-20  align:#left
	spinner 'Diff_G' "G" pos:[120,160] width:80 height:16 range:[0,1,1] scale:1e-20  align:#left
	spinner 'Diff_B' "B" pos:[216,160] width:81 height:16 range:[0,1,1] scale:1e-20  align:#left
	spinner 'Spec_R' "R" pos:[328,160] width:80 height:16 range:[0,1,1] scale:1e-20  align:#left
	spinner 'Spec_G' "G" pos:[416,160] width:80 height:16 range:[0,1,1] scale:1e-20  align:#left
	spinner 'Spec_B' "B" pos:[512,160] width:81 height:16 range:[0,1,1] scale:1e-20  align:#left
	spinner 'GlossMulti' "" pos:[632,160] width:88 height:16 align:#left range:[0,255,255]
	groupBox 'grp6' "Texture Maps" pos:[24,192] width:440 height:128 align:#left
	groupBox 'TileBoxs' "Tiling" pos:[472,192] width:280 height:64 align:#left
	editText 'TexMap_Diff' "Diff" pos:[40,208] width:408 height:20 align:#left
	editText 'TexMap_Spec' "Spec" pos:[32,240] width:416 height:20 align:#left
	editText 'TexMap_ddna' "DDNA" pos:[24,272] width:424 height:20 align:#left
	spinner 'TileU' "Tile U" pos:[504,208] width:68 height:16 range:[0,1000,8] scale:0.0001 align:#left
	spinner 'TileV' "Tile V" pos:[624,208] width:69 height:16 range:[0,1000,8] scale:0.0001 align:#left
	spinner 'OffU' "Offset U  " pos:[480,232] width:75 height:16 range:[0,1000,0] scale:0.0001 align:#left
	spinner 'OffV' "Offset V  " pos:[616,232] width:76 height:16 range:[0,1000,0] scale:0.0001 align:#left
	button 'btn_make' "Load Layer" pos:[610,328] width:150 height:32 align:#left
	groupBox 'grp8' "Map Output Scale" pos:[472,256] width:280 height:64 align:#left
	spinner 'Scale_diff' "" pos:[481,296] width:72 height:16 range:[0,1,0.5] scale:0.0001 align:#left
	spinner 'Scale_gloss' "" pos:[569,296] width:72 height:16 range:[0,1,0.5] scale:0.0001 align:#left
	spinner 'Scale_normal' "" pos:[664,296] width:72 height:16 range:[0,1,0.5] scale:0.0001 align:#left
	label 'lbl1' "diff" pos:[496,280] width:24 height:16 align:#left
	label 'lbl2' "gloss" pos:[592,280] width:24 height:16 align:#left
	label 'lbl3' "normal" pos:[680,280] width:48 height:16 align:#left
	bitmap 'bmp3' "Bitmap" pos:[648,8] width:80 height:80 align:#left fileName:"Logo_Community_logo.tiff"
	spinner 'IORspn' "IOR" pos:[447,104] width:83 height:16 align:#left range: [0,100,1.6]
	spinner 'Metal' "Metalness" pos:[600,104] width:83 height:16 align:#left range: [0,1,0]
	dropdownlist  'NamerPre' "Layer Prefix" pos:[20,10] width:160 height:50 align:#left readonly:true selection:1 items:#("highresolution_dielectric","highresolution_metal","highresolution_metallic","highresolution_synthetic",
		"lowresolution_fabric", "lowresolution_metal", "lowresolution_natural", "lowresolution_synthetic", "_reference")
	button 'ResetReboot' "Reset" pos:[530,328] width:80 height:32 align:#left
 	spinner 'NTileU' "Normal Tile U" pos:[74,295] width:68 height:16 range:[0,1000,8] scale:0.0001 align:#left
	spinner 'NTileV' "Normal Tile V" pos:[254,295] width:69 height:16 range:[0,1000,8] scale:0.0001 align:#left
	checkbox 'DiffuseApply' "Apply Diffuse Color" checked:off  pos:[130,125] width:120 height:18
	checkbox 'Diff_Invert' "Invert Diffuse Color" checked:off  pos:[530,80] width:328 height:18	
	checkbox 'ClearCoat' "Clear Coat" checked:off pos:[350,320] width:80 height:22 
	spinner 'CCIor' "IOR" range:[0,100,1.5304] pos:[360, 345] width:110 height: 22
		
	on btn_Make pressed do
	(
		--create materials
		if Mat_Name.text != "" do
		(
			meditMaterials[activeMeditSlot]=VRayMtl()
			meditMaterials[activeMeditSlot].name=(NamerPre.text+"_"+Mat_Name.text+".mtl")
			meditMaterials[activeMeditSlot].refraction_ior=IORspn.value
			meditMaterials[activeMeditSlot].reflection_metalness=Metal.value
			meditMaterials[activeMeditSlot].Diffuse=color (Diff_R.value*255) (Diff_G.value*255) (Diff_B.value*255) 
			meditMaterials[activeMeditSlot].Reflection=color (Spec_R.value*255) (Spec_G.value*255) (Spec_B.value*255)
			meditMaterials[activeMeditSlot].texmap_diffuse=CompositeTexturemap()
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1]=CompositeTexturemap()
			meditMaterials[activeMeditSlot].texmap_bump_multiplier = 100
			meditMaterials[activeMeditSlot].texmap_reflection=CompositeTexturemap()
			meditMaterials[activeMeditSlot].texmap_reflection.maplist[2]=ColorCorrection()
			meditMaterials[activeMeditSlot].texmap_reflection.maplist[2].color=color (Spec_R.value*255) (Spec_G.value*255) (Spec_B.value*255) 255
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness=CompositeTexturemap()
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1]=CompositeTexturemap()
			meditMaterials[activeMeditSlot].texmap_bump=VRayNormalMap()			
		)
		
		if DiffuseApply.state==true do
		(
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].maplist[2]=ColorCorrection()
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].maplist[2].color=color (Diff_R.value*255) (Diff_G.value*255) (Diff_B.value*255) 255
		)
		
		if DiffuseApply.state==true and Diff_Invert.state==true do
		(
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].maplist[2]=ColorCorrection()
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].maplist[2].rewireR=4
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].maplist[2].rewireG=5
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].maplist[2].rewireB=6
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].maplist[2].rewireA=3
		)
		
		if TexMap_Diff.text !=""do
		(
		--diffuse

			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].maplist[1]=VRayBitmap()
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].maplist[1].HDRIMapName=(Texture_SRC.text+TexMap_Diff.text)

			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].blendMode[2]=5
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].opacity[1]=(Scale_diff.value *100)
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].maplist[1].coords.U_Offset = OffU.value
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].maplist[1].coords.V_Offset = OffV.value
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling = TileU.value
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling = TileV.value
		)
		--spec
		if TexMap_Spec.text !="" do
			(
				meditMaterials[activeMeditSlot].texmap_reflection.maplist[1]=VRayBitmap()

				meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].HDRIMapName=(Texture_SRC.text+TexMap_Spec.text)
				meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].color_space=0
				meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].coords.U_Offset = OffU.value
				meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].coords.V_Offset = OffV.value
				meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].coords.U_Tiling = TileU.value
				meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].coords.V_Tiling = TileV.value
				meditMaterials[activeMeditSlot].texmap_reflection.blendMode[2]=5
			)
		if TexMap_Spec.text=="" do
		(
		meditMaterials[activeMeditSlot].texmap_reflection=CompositeTexturemap()
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[2]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[2].color=color (Spec_R.value*255) (Spec_G.value*255) (Spec_B.value*255) 255
		meditMaterials[activeMeditSlot].texmap_reflection.blendMode[2]=5			
		)
		--gloss
		if TexMap_ddna.text !="" do
			(
			TNormString=TexMap_ddna.text
			TexMap_ddna_gloss=substring TNormString 1 (TNormString.count-4)+"_glossMap.tif"

			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1]=VRayBitmap()
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].HDRIMapName=(Texture_SRC.text+TexMap_ddna_gloss)
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].color_space=0
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Offset = 0
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Offset = 0
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling = NTileU.value
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling = NTileV.value
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.opacity[1]=((GlossMulti.value/255)*100)
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].opacity[1]=(Scale_gloss.value *100)
			--ddna

			meditMaterials[activeMeditSlot].texmap_bump.bump_map=VRayBitmap()
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.HDRIMapName=(Texture_SRC.text+TexMap_ddna.text)
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.color_space=0
			meditMaterials[activeMeditSlot].texmap_bump.bump_map_multiplier=Scale_normal.value
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.coords.U_Offset = 0
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.coords.V_Offset = 0
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.coords.U_Tiling = NTileU.value
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.coords.V_Tiling = NTileV.value
			)
			
		if ClearCoat.state==on do
		(
		meditMaterials[activeMeditSlot].coat_amount = 1
		meditMaterials[activeMeditSlot].coat_bump_lock = on
		meditMaterials[activeMeditSlot].coat_glossiness=(GlossMulti.value/255)
		meditMaterials[activeMeditSlot].coat_ior=CCIor.value 
		)
		
		if TexMap_ddna.text =="" do
		(
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1]=colorcorrection()
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].color=color GlossMulti.value GlossMulti.value GlossMulti.value 255
 		)
	)
	

	on ResetReboot pressed do
	(
		Mat_Name.text=""
		Diff_R.value=1
		Diff_G.value=1
		Diff_B.value=1
		Spec_R.value=1
		Spec_G.value=1
		Spec_B.value=1
		GlossMulti.value=255
		TexMap_Diff.text=""
		TexMap_Spec.text=""
		TexMap_ddna.text=""
		TileU.value=8
		TileV.value=8
		OffU.value=0
		OffV.value=0
		Scale_diff.value=0.5
		Scale_gloss.value=0.5
		Scale_normal.value=0.5
		IORspn.value=1.6
		Metal.value=0
		NTileU.value=8
		NTileV.value=8
		Diff_Invert.checked=off
		DiffuseApply.checked=off
		ClearCoat.state==off
	)

)
createDialog SC_layerimporter
