try (closeRolloutFloater StencilDecal) catch()

maxINI = getMAXIniFile()
tex_path = getINISetting maxINI "SCTools" "tex_path"


rollout StencilDecal "SC Stencil Decal Importer alpha 0.0.1" width:743 height:365
(

	
	bitmap 'bmp1' "Bitmap" pos:[170,3] width:400 height:80 fileName:"sc_stencil.tif" align:#left
	editText 'TexMap_Sten' "Stencil Texture" pos:[77,279] width:555 height:20 align:#left
	editText 'TexPath' "Texture Path:" pos:[75,326] width:302 height:20 align:#left text:tex_path --SET THIS TO YOUR TEXTURE PATH
	button 'ImportDecal' "Import Decal" pos:[539,315] width:159 height:37 align:#left
	groupBox 'grp1' "Color 1" pos:[9,85] width:177 height:180 align:#left
	groupBox 'grp2' "Color 2" pos:[192,85] width:177 height:180 align:#left
	groupBox 'grp3' "Color 3" pos:[374,85] width:177 height:180 align:#left
	groupBox 'grp4' "Color 4" pos:[557,85] width:177 height:180 align:#left
	label 'lbl1' "Diff [1.0]" pos:[84,93] width:48 height:16 align:#left
	label 'lbl2' "Diff [1.0]" pos:[271,96] width:50 height:16 align:#left
	label 'lbl3' "Diff [1.0]" pos:[449,96] width:50 height:16 align:#left
	label 'lbl4' "Diff [1.0]" pos:[637,96] width:50 height:16 align:#left
	spinner 'C1_Diff_R' "R" pos:[41,111] width:112 height:16 align:#left
	spinner 'C1_Diff_G' "G" pos:[41,131] width:112 height:16 range:[0,1,0] align:#left scale:0.0000001
	spinner 'C1_Diff_B' "B" pos:[41,150] width:113 height:16 range:[0,1,0] align:#left scale:0.0000001
	spinner 'C2_Diff_R' "R" pos:[223,113] width:112 height:16 range:[0,1,0] align:#left scale:0.0000001
	spinner 'C2_Diff_G' "G" pos:[223,133] width:112 height:16 range:[0,1,0] align:#left scale:0.0000001
	spinner 'C2_Diff_B' "B" pos:[223,152] width:113 height:16 range:[0,1,0] align:#left scale:0.0000001
	spinner 'C3_Diff_R' "R" pos:[408,113] width:112 height:16 range:[0,1,0] align:#left scale:0.0000001
	spinner 'C3_Diff_G' "G" pos:[408,133] width:112 height:16 range:[0,1,0] align:#left scale:0.0000001
	spinner 'C3_Diff_B' "B" pos:[408,152] width:113 height:16 range:[0,1,0] align:#left scale:0.0000001
	spinner 'C4_Diff_R' "R" pos:[591,113] width:112 height:16 range:[0,1,0] align:#left scale:0.0000001
	spinner 'C4_Diff_G' "G" pos:[591,133] width:112 height:16 range:[0,1,0] align:#left scale:0.0000001
	spinner 'C4_Diff_B' "B" pos:[591,152] width:113 height:16 range:[0,1,0] align:#left scale:0.0000001
	label 'lbl5' "Spec [1.0]" pos:[74,168] width:54 height:15 align:#left
	label 'lbl6' "Spec [1.0]" pos:[260,170] width:54 height:15 align:#left
	label 'lbl7' "Spec [1.0]" pos:[442,170] width:54 height:15 align:#left
	label 'lbl8' "Spec [1.0]" pos:[626,170] width:54 height:15 align:#left
	spinner 'C1_Spec_R' "R" pos:[44,188] width:112 height:16 range:[0,1,1] align:#left scale:0.0000001
	spinner 'C1_Spec_G' "G" pos:[44,208] width:112 height:16 range:[0,1,1] align:#left scale:0.0000001
	spinner 'C1_Spec_B' "B" pos:[44,227] width:113 height:16 range:[0,1,1] align:#left scale:0.0000001
	spinner 'C2_Spec_R' "R" pos:[226,190] width:112 height:16 range:[0,1,1] align:#left scale:0.0000001
	spinner 'C2_Spec_G' "G" pos:[226,208] width:112 height:16 range:[0,1,1] align:#left scale:0.0000001
	spinner 'C2_Spec_B' "B" pos:[226,227] width:113 height:16 range:[0,1,1] align:#left scale:0.0000001
	spinner 'C3_Spec_R' "R" pos:[411,190] width:112 height:16 range:[0,1,1] align:#left scale:0.0000001
	spinner 'C3_Spec_G' "G" pos:[411,210] width:112 height:16 range:[0,1,1] align:#left scale:0.0000001
	spinner 'C3_Spec_B' "B" pos:[411,229] width:113 height:16 range:[0,1,1] align:#left scale:0.0000001
	spinner 'C4_Spec_R' "R" pos:[594,190] width:112 height:16 range:[0,1,1] align:#left scale:0.0000001
	spinner 'C4_Spec_G' "G" pos:[594,210] width:112 height:16 range:[0,1,1] align:#left scale:0.0000001
	spinner 'C4_Spec_B' "B" pos:[594,229] width:113 height:16 range:[0,1,1] align:#left scale:0.0000001
	spinner 'Gloss' "Gloss" pos:[400,328] width:101 height:16 align:#left range:[0,1,0.5] scale:0.0000001
	edittext 'name' "Name:" pos:[15,50] width:140 height:20 align:#left
	edittext 'Matname' "MTL Name" pos:[10,25] width:140 height:20 align:#left
	
	on ImportDecal pressed do
	(
		
		meditMaterials[activeMeditSlot]=VRayMtl()
		meditMaterials[activeMeditSlot].name=(Matname.text+"_mtl_"+name.text)
		meditMaterials[activeMeditSlot].texmap_diffuse=CompositeTexturemap()
		meditMaterials[activeMeditSlot].texmap_reflection=CompositeTexturemap()
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness=CompositeTexturemap()
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.mask[1]=CompositeTexturemap()
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.opacity[1]=((Gloss.value/255)*100)
		meditMaterials[activeMeditSlot].texmap_opacity=CompositeTexturemap()
		
		--diffuse composite color
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[4]=ColorCorrection()
		
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].color=color (C1_Diff_R.value*255) (C1_Diff_G.value*255) (C1_Diff_B.value*255) 255
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].color=color (C2_Diff_R.value*255) (C2_Diff_G.value*255) (C2_Diff_B.value*255) 255
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].color=color (C3_Diff_R.value*255) (C3_Diff_G.value*255) (C3_Diff_B.value*255) 255
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[4].color=color  (C4_Diff_R.value*255) (C4_Diff_G.value*255) (C4_Diff_B.value*255) 255
		
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2]=CompositeTextureMap()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].rewireR=0
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].rewireG=10
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].rewireB=10
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].rewireA=9
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].map=VRayBitmap()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].map.HDRIMapName=(TexPath.text+TexMap_Sten.text)
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].map.color_space=0.0
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].blendmode[1]=0
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[2]=RGB_Tint()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].blendMode[2]=23
				
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3]=CompositeTextureMap()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[1].rewireR=10
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[1].rewireG=1
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[1].rewireB=10
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[1].rewireA=9
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[1].map=VRayBitmap()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[1].map.HDRIMapName=(TexPath.text+TexMap_Sten.text)
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[1].map.color_space=0.0
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].blendmode[1]=0
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[2]=RGB_Tint()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].blendMode[2]=23
			
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4]=CompositeTextureMap()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[1].rewireR=10
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[1].rewireG=10
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[1].rewireB=2
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[1].rewireA=9
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[1].map=VRayBitmap()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[1].map.HDRIMapName=(TexPath.text+TexMap_Sten.text)
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[1].map.color_space=0.0
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].blendmode[1]=0
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[2]=RGB_Tint()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].blendMode[2]=23
		
		--spec maps
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[2]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[3]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[4]=ColorCorrection()
		
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].color=color (C1_Spec_R.value*255) (C1_Spec_G.value*255) (C1_Spec_B.value*255) 255
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[2].color=color (C2_Spec_R.value*255) (C2_Spec_G.value*255) (C2_Spec_B.value*255) 255
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[3].color=color (C3_Spec_R.value*255) (C3_Spec_G.value*255) (C3_Spec_B.value*255) 255
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[4].color=color  (C4_Spec_R.value*255) (C4_Spec_G.value*255) (C4_Spec_B.value*255) 255
		
		meditMaterials[activeMeditSlot].texmap_reflection.mask[2]=CompositeTextureMap()
		meditMaterials[activeMeditSlot].texmap_reflection.mask[2].maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflection.mask[2].maplist[1].rewireR=0
		meditMaterials[activeMeditSlot].texmap_reflection.mask[2].maplist[1].rewireG=10
		meditMaterials[activeMeditSlot].texmap_reflection.mask[2].maplist[1].rewireB=10
		meditMaterials[activeMeditSlot].texmap_reflection.mask[2].maplist[1].rewireA=9
		meditMaterials[activeMeditSlot].texmap_reflection.mask[2].maplist[1].map=VRayBitmap()
		meditMaterials[activeMeditSlot].texmap_reflection.mask[2].maplist[1].map.HDRIMapName=(TexPath.text+TexMap_Sten.text)
		meditMaterials[activeMeditSlot].texmap_reflection.mask[2].maplist[1].map.color_space=0.0
		meditMaterials[activeMeditSlot].texmap_reflection.mask[2].blendmode[1]=0
		meditMaterials[activeMeditSlot].texmap_reflection.mask[2].maplist[2]=RGB_Tint()
		meditMaterials[activeMeditSlot].texmap_reflection.mask[2].blendMode[2]=23
				
		meditMaterials[activeMeditSlot].texmap_reflection.mask[3]=CompositeTextureMap()
		meditMaterials[activeMeditSlot].texmap_reflection.mask[3].maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflection.mask[3].maplist[1].rewireR=10
		meditMaterials[activeMeditSlot].texmap_reflection.mask[3].maplist[1].rewireG=1
		meditMaterials[activeMeditSlot].texmap_reflection.mask[3].maplist[1].rewireB=10
		meditMaterials[activeMeditSlot].texmap_reflection.mask[3].maplist[1].rewireA=9
		meditMaterials[activeMeditSlot].texmap_reflection.mask[3].maplist[1].map=VRayBitmap()
		meditMaterials[activeMeditSlot].texmap_reflection.mask[3].maplist[1].map.HDRIMapName=(TexPath.text+TexMap_Sten.text)
		meditMaterials[activeMeditSlot].texmap_reflection.mask[3].maplist[1].map.color_space=0.0
		meditMaterials[activeMeditSlot].texmap_reflection.mask[3].blendmode[1]=0
		meditMaterials[activeMeditSlot].texmap_reflection.mask[3].maplist[2]=RGB_Tint()
		meditMaterials[activeMeditSlot].texmap_reflection.mask[3].blendMode[2]=23
			
		meditMaterials[activeMeditSlot].texmap_reflection.mask[4]=CompositeTextureMap()
		meditMaterials[activeMeditSlot].texmap_reflection.mask[4].maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflection.mask[4].maplist[1].rewireR=10
		meditMaterials[activeMeditSlot].texmap_reflection.mask[4].maplist[1].rewireG=10
		meditMaterials[activeMeditSlot].texmap_reflection.mask[4].maplist[1].rewireB=2
		meditMaterials[activeMeditSlot].texmap_reflection.mask[4].maplist[1].rewireA=9
		meditMaterials[activeMeditSlot].texmap_reflection.mask[4].maplist[1].map=VRayBitmap()
		meditMaterials[activeMeditSlot].texmap_reflection.mask[4].maplist[1].map.HDRIMapName=(TexPath.text+TexMap_Sten.text)
		meditMaterials[activeMeditSlot].texmap_reflection.mask[4].maplist[1].map.color_space=0.0
		meditMaterials[activeMeditSlot].texmap_reflection.mask[4].blendmode[1]=0
		meditMaterials[activeMeditSlot].texmap_reflection.mask[4].maplist[2]=RGB_Tint()
		meditMaterials[activeMeditSlot].texmap_reflection.mask[4].blendMode[2]=23
		
		--gloss
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1]=CompositeTextureMap()
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].color=color 255 255 255 255
		
		--opac
		meditMaterials[activeMeditSlot].texmap_opacity.maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_opacity.maplist[2]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_opacity.maplist[3]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_opacity.maplist[4]=ColorCorrection()
		
		meditMaterials[activeMeditSlot].texmap_opacity.maplist[1].color=color 255 255 255 255 
		meditMaterials[activeMeditSlot].texmap_opacity.maplist[2].color=color 255 255 255 255 
		meditMaterials[activeMeditSlot].texmap_opacity.maplist[3].color=color 255 255 255 255 
		meditMaterials[activeMeditSlot].texmap_opacity.maplist[4].color=color 255 255 255 255
		
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2]=CompositeTextureMap()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[1].rewireR=0
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[1].rewireG=10
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[1].rewireB=10
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[1].rewireA=9
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[1].map=VRayBitmap()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[1].map.HDRIMapName=(TexPath.text+TexMap_Sten.text)
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[1].map.color_space=0.0
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].blendmode[1]=0
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[2]=RGB_Tint()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].blendMode[2]=23
				
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3]=CompositeTextureMap()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[1].rewireR=10
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[1].rewireG=1
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[1].rewireB=10
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[1].rewireA=9
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[1].map=VRayBitmap()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[1].map.HDRIMapName=(TexPath.text+TexMap_Sten.text)
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[1].map.color_space=0.0
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].blendmode[1]=0
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[2]=RGB_Tint()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].blendMode[2]=23
			
		meditMaterials[activeMeditSlot].texmap_opacity.mask[4]=CompositeTextureMap()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[4].maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[4].maplist[1].rewireR=10
		meditMaterials[activeMeditSlot].texmap_opacity.mask[4].maplist[1].rewireG=10
		meditMaterials[activeMeditSlot].texmap_opacity.mask[4].maplist[1].rewireB=2
		meditMaterials[activeMeditSlot].texmap_opacity.mask[4].maplist[1].rewireA=9
		meditMaterials[activeMeditSlot].texmap_opacity.mask[4].maplist[1].map=VRayBitmap()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[4].maplist[1].map.HDRIMapName=(TexPath.text+TexMap_Sten.text)
		meditMaterials[activeMeditSlot].texmap_opacity.mask[4].maplist[1].map.color_space=0.0
		meditMaterials[activeMeditSlot].texmap_opacity.mask[4].blendmode[1]=0
		meditMaterials[activeMeditSlot].texmap_opacity.mask[4].maplist[2]=RGB_Tint()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[4].blendMode[2]=23
		
		meditMaterials[activeMeditSlot].option_opacityMode=0
	)
	
)

CreateDialog StencilDecal
