try (closeRolloutFloater scLightload) catch()

maxINI = getMAXIniFile()
script_path = getINISetting maxINI "SCTools" "script_path"

rollout scLightload "StarFab Lightloader" width:564 height:592
(
	
	bitmap 'bmp1' "Logo" pos:[185,57] width:162 height:150 enabled:true fileName:"StarFabPython\Logo_Community.tiff" align:#left
	button 'btn1' "Default State" pos:[65,462] width:120 height:60 align:#left
	button 'btn2' "Emergency State" pos:[341,463] width:130 height:60 align:#left
	button 'btn4' "Auxiliary State" pos:[198,462] width:130 height:60 align:#left
	label 'lbl1' "Created by Baconator with massive help from Ventorvar" pos:[133,33] width:395 height:24 align:#left
	label 'lbl2' "HOW TO USE" pos:[229,220] width:68 height:21 align:#left
	label 'lbl3' "1.) Create a New Scene" pos:[37,235] width:471 height:29 align:#left
	label 'lbl4' "2.) Click which light mode you wish to load in" pos:[38,252] width:471 height:18 align:#left
	label 'lbl5' "3.) Save scene as **_light_.max'" pos:[38,268] width:463 height:17 align:#left
	label 'lbl6' "example: Arrow_Default_Lights.max" pos:[78,283] width:471 height:22 align:#left
	label 'lbl7' "StarFab 3ds MAX Stage 1 Light Loader" pos:[172,5] width:393 height:15 align:#left
	label 'lbl8' "4.) Load your 'shipmodel/ObjectContainer' OR Create a new Scene in load a fbx file " pos:[36,298] width:513 height:17 align:#left
	label 'lbl9' "(TIP: Use Blender to load a ships .sbcp and then export it out)" pos:[76,314] width:513 height:17 align:#left
	label 'lbl10' "5.) File->Import->Replace [Merge your ***_light.max] " pos:[36,329] width:513 height:17 align:#left
	label 'lbl11' "-Lights that are loaded in are missing their transforms (Position and Rotation) this is due to maxscript having issues reading the data in the scbp." pos:[41,381] width:508 height:33 align:#left
	label 'lbl12' "Known Issues" pos:[217,357] width:109 height:18 align:#left
	label 'lbl13' "-Lights that do not use a projection map are loading with bitmaps with incomplete paths (there is no file). These will need to be removed by hand OR use Bitmap Tracking/Resizing (paid script) to remove the path, then use lost_textures (by Oscar Romero/included). Close and Reopen to load another scbp" pos:[39,413] width:492 height:45 align:#left
	button 'btn5' "Step 3: lost_textures" pos:[274,538] width:139 height:42 align:#left
	button 'btn6' "Step 2: Bitmap Trackg/Resizing (Must be installed/paid app)" pos:[108,537] width:139 height:42 align:#left
	on btn1 pressed  do
		python.ExecuteFile script_path + "StarFabPython\StarFab_Max_LightMaker_01_default.py"
	on btn2 pressed  do
		python.ExecuteFile script_path + "StarFabPython\StarFab_Max_LightMaker_01_Emergency.py"
	on btn4 pressed  do
		python.ExecuteFile script_path + "StarFabPython\StarFab_MAX_LightMaker_01_Auxilary.py"
	on btn5 pressed  do
		fileIn "StarFabPython\lost_textures.ms"
	on btn6 pressed  do
		fileIn ((getDir #userScripts)+"\\Pixamoon\\pxm-BitmapTracking-Resizing.mse") quiet:true
)
createDialog scLightload
