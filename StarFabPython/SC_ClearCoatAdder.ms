try (closeRolloutFloater SC_ClearCoatAdder) catch()

rollout SC_ClearCoatAdder "Clear Coat Adder" width:300 height:150
(
	spinner 'IORmount' "IOR"pos:[130,30] width:60 height:20 range:[0,100,1.5304] 
	button 'Adjust' "Add" pos: [100, 60] width: 120 height: 40
	label 'Lb1' "IOR examples; Expoxy:1.5304, Polyurethane: 1.5," width: 270 height:60 pos: [10, 100]
	
	on Adjust pressed do
	(
		meditMaterials[activeMeditSlot].coat_amount = 1
		meditMaterials[activeMeditSlot].coat_bump_lock = on
		meditMaterials[activeMeditSlot].texmap_coat_glossiness=meditMaterials[activeMeditSlot].texmap_reflectionGlossiness
		meditMaterials[activeMeditSlot].coat_ior=IORmount.value 
		
	)
)
CreateDialog SC_ClearCoatAdder