maxINI = getMAXIniFile()
tex_path = getINISetting maxINI "SCTools" "tex_path"
cache_path = getINISetting maxINI "SCTools" "cache_path"
script_path = getINISetting maxINI "SCTools" "script_path"
json_path = getINISetting maxINI "SCTools" "json_path"

rollout SCGlow "Untitled" width:616 height:300
(


	bitmap 'bmp1' "Bitmap" pos:[107,8] width:380 height:60 fileName:"glow.tif" align:#left
	label 'lbl3' "Emission Color" pos:[472,128] width:88 height:16 align:#left
	groupBox 'lb_name' "Name" pos:[24,72] width:376 height:72 align:#left
	groupBox 'box_Glow' "Glow Properties" pos:[416,72] width:184 height:168 align:#left
	groupBox 'grp4' "Map Paths" pos:[24,152] width:376 height:136 align:#left
	button 'btn_make' "Import Material" pos:[428,249] width:170 height:38 align:#left
	editText 'Name_MTL' "MTL Name" pos:[56,88] width:208 height:20 align:#left
	editText 'Material_Name' "Material Name" pos:[40,112] width:280 height:20 align:#left
	editText 'TexMap_Diff' "Diffuse Map" pos:[40,176] width:328 height:20 align:#left
	editText 'TexPath' "Texture Path" pos:[40,240] width:328 height:20 align:#left text:tex_path--SET THIS TO YOUR TEXTURE PATH
	checkbox 'chk_alpha' "Apply Alpha to Opac" pos:[104,200] width:120 height:24 align:#left
	spinner 'GlowAmount' "Glow Amount" pos:[432,96] width:57 height:16 align:#left
	spinner 'Illum_R' "R" pos:[448,152] width:103 height:16 range:[0,1,0] scale:0.0001 align:#left
	spinner 'Illum_G' "G" pos:[448,176] width:103 height:16 range:[0,1,0] scale:0.0001 align:#left
	spinner 'Illum_B' "B" pos:[448,200] width:104 height:16 range:[0,1,0] scale:0.0001 align:#left
	button 'GML_Transfer' "Transfer from GML" pos:[280,87] width:100 height:20 align:#left
	editText 'TexMap_Displ' "Displacement Map" pos:[40,207] width:328 height:20 align:#left
	spinner 'Displacement Amount' "Displacement Amount" pos:[130,235] width:103 height:16 range:[0,1,0] scale:0.0001 align:#left
	button 'Illum_Illum' "EMIS" pos:[430,128] width:33 height:16 
	button 'Illum_Diff' "DIFF" pos:[550,128] width:33 height:16 


	on GML_Transfer pressed do
	(
		mtl_in_filet = openFile "T://SC_Cache//BaseOutput.txt" mode: "rt" --SET THIS TO TEMP LOCATION
		
		mtlname=execute(readline mtl_in_filet)
		Base_diff=execute(readline mtl_in_filet) 
		Base_ddna=execute(readline mtl_in_filet) 
		Base_detail=execute(readline mtl_in_filet)
		Base_spec=execute(readline mtl_in_filet)
		BaseB_diff=execute(readline mtl_in_filet)
		Base_disp=execute(readline mtl_in_filet)
		BaseB_ddna=execute(readline mtl_in_filet)
		BaseB_detail=execute(readline mtl_in_filet)
		BaseB_spec=execute(readline mtl_in_filet)
		Base_mask=execute(readline mtl_in_filet)
		Base_SpecR=execute(readline mtl_in_filet)
		Base_SpecG=execute(readline mtl_in_filet)
		Base_SpecB=execute(readline mtl_in_filet)
		BaseB_SpecR=execute(readline mtl_in_filet)
		BaseB_SpecG=execute(readline mtl_in_filet)
		BaseB_SpecB=execute(readline mtl_in_filet)
		Base_DiffR=execute(readline mtl_in_filet)
		Base_DiffG=execute(readline mtl_in_filet)
		Base_DiffB=execute(readline mtl_in_filet)
		Base2_DiffR=execute(readline mtl_in_filet)
		Base2_DiffG=execute(readline mtl_in_filet)
		Base2_DiffB=execute(readline mtl_in_filet)
		Base_BlendLayerTile=execute(readline mtl_in_filet)
		Base_WearBlendBase=execute(readline mtl_in_filet)
		Base_BlendFactor=execute(readline mtl_in_filet)
		Base_Gloss=execute(readline mtl_in_filet)
		Base2_Gloss=execute(readline mtl_in_filet)
		Base_Tile=execute(readline mtl_in_filet)
		Base_Detail_DiffScale=execute(readline mtl_in_filet)
		Base2_Detail_DiffScale=execute(readline mtl_in_filet)
		Base_Detail_Nrm=execute(readline mtl_in_filet)
		BaseB_Detail_Nrm=execute(readline mtl_in_filet)
		Base_Detail_Gloss=execute(readline mtl_in_filet)
		BaseB_Detail_Gloss=execute(readline mtl_in_filet)
		Base_TintR=execute(readline mtl_in_filet)
		Base_TintG=execute(readline mtl_in_filet)
		Base_TintB=execute(readline mtl_in_filet)
		Base2_TintR=execute(readline mtl_in_filet)
		Base2_TintG=execute(readline mtl_in_filet)
		Base2_TintB=execute(readline mtl_in_filet)
		Base_LayerGloss=execute(readline mtl_in_filet)
		Base2_LayerGloss=execute(readline mtl_in_filet)
		Base_Layer1UV=execute(readline mtl_in_filet)
		Base2_Layer2UV=execute(readline mtl_in_filet)
		LayerTF2=execute(readline mtl_in_filet)
		LayerTF1=execute(readline mtl_in_filet)
		DisplOutput=execute(readline mtl_in_filet)
		TintPallete_Base=execute(readline mtl_in_filet)
		TintPallete_Blend=execute(readline mtl_in_filet)
		Base_Detail_TileU=execute(readline mtl_in_filet)
		Base_Detail_TileV=execute(readline mtl_in_filet)
		Blend_Detail_TileU=execute(readline mtl_in_filet)
		Blend_Detail_TileV=execute(readline mtl_in_filet)
		IllumRz=execute(readline mtl_in_filet)
		IllumGz=execute(readline mtl_in_filet)
		IllumBz=execute(readline mtl_in_filet)
		Glow=execute(readline mtl_in_filet)
		Glass_R=execute(readline mtl_in_filet)
		Glass_G=execute(readline mtl_in_filet)
		Glass_B=execute(readline mtl_in_filet)
		TintCloudiness=execute(readline mtl_in_filet)

		if mtlname  !="" do
		(
		Material_Name.text=mtlname as string
		)
		
		if Base_diff  !="" do
		(
		TexMap_Diff.text=Base_diff as string
		)
		
		
		if Glow  !="" do
		(
		GlowAmount.value=Glow as float
		)
		
		
		if Illum_Rz  !="" do
		(
			try(
		IllumR.value=IllumRz as float
			)
			catch()
		)
		
		if Illum_Gz  !="" do
		(
			try(
		IllumG.value=IllumGz as float)
		catch()
		)
		
		if Illum_Bz  !="" do
		(try
			(
		IllumR.value=IllumBz as float
			)
			catch()
		)
		
		
		if Base_disp  !="" do
		(
		TexMap_Displ.value=Base_disp as float
		)
		
		if DisplOutput  !="" do
		(
		DisplacementAmount.value=DisplOutput as float
		)
		close mtl_in_filet
	)
	
	on Illum_Illum pressed do
	(
		mtl_in_filet = openFile "T://SC_Cache//BaseOutput.txt" mode: "rt" --SET THIS TO TEMP LOCATION
		
		mtlname=execute(readline mtl_in_filet)
		Base_diff=execute(readline mtl_in_filet) 
		Base_ddna=execute(readline mtl_in_filet) 
		Base_detail=execute(readline mtl_in_filet)
		Base_spec=execute(readline mtl_in_filet)
		BaseB_diff=execute(readline mtl_in_filet)
		Base_disp=execute(readline mtl_in_filet)
		BaseB_ddna=execute(readline mtl_in_filet)
		BaseB_detail=execute(readline mtl_in_filet)
		BaseB_spec=execute(readline mtl_in_filet)
		Base_mask=execute(readline mtl_in_filet)
		Base_SpecR=execute(readline mtl_in_filet)
		Base_SpecG=execute(readline mtl_in_filet)
		Base_SpecB=execute(readline mtl_in_filet)
		BaseB_SpecR=execute(readline mtl_in_filet)
		BaseB_SpecG=execute(readline mtl_in_filet)
		BaseB_SpecB=execute(readline mtl_in_filet)
		Base_DiffR=execute(readline mtl_in_filet)
		Base_DiffG=execute(readline mtl_in_filet)
		Base_DiffB=execute(readline mtl_in_filet)
		Base2_DiffR=execute(readline mtl_in_filet)
		Base2_DiffG=execute(readline mtl_in_filet)
		Base2_DiffB=execute(readline mtl_in_filet)
		Base_BlendLayerTile=execute(readline mtl_in_filet)
		Base_WearBlendBase=execute(readline mtl_in_filet)
		Base_BlendFactor=execute(readline mtl_in_filet)
		Base_Gloss=execute(readline mtl_in_filet)
		Base2_Gloss=execute(readline mtl_in_filet)
		Base_Tile=execute(readline mtl_in_filet)
		Base_Detail_DiffScale=execute(readline mtl_in_filet)
		Base2_Detail_DiffScale=execute(readline mtl_in_filet)
		Base_Detail_Nrm=execute(readline mtl_in_filet)
		BaseB_Detail_Nrm=execute(readline mtl_in_filet)
		Base_Detail_Gloss=execute(readline mtl_in_filet)
		BaseB_Detail_Gloss=execute(readline mtl_in_filet)
		Base_TintR=execute(readline mtl_in_filet)
		Base_TintG=execute(readline mtl_in_filet)
		Base_TintB=execute(readline mtl_in_filet)
		Base2_TintR=execute(readline mtl_in_filet)
		Base2_TintG=execute(readline mtl_in_filet)
		Base2_TintB=execute(readline mtl_in_filet)
		Base_LayerGloss=execute(readline mtl_in_filet)
		Base2_LayerGloss=execute(readline mtl_in_filet)
		Base_Layer1UV=execute(readline mtl_in_filet)
		Base2_Layer2UV=execute(readline mtl_in_filet)
		LayerTF2=execute(readline mtl_in_filet)
		LayerTF1=execute(readline mtl_in_filet)
		DisplOutput=execute(readline mtl_in_filet)
		TintPallete_Base=execute(readline mtl_in_filet)
		TintPallete_Blend=execute(readline mtl_in_filet)
		Base_Detail_TileU=execute(readline mtl_in_filet)
		Base_Detail_TileV=execute(readline mtl_in_filet)
		Blend_Detail_TileU=execute(readline mtl_in_filet)
		Blend_Detail_TileV=execute(readline mtl_in_filet)
		IllumRz=execute(readline mtl_in_filet)
		IllumGz=execute(readline mtl_in_filet)
		IllumBz=execute(readline mtl_in_filet)
		Glow=execute(readline mtl_in_filet)
		Glass_R=execute(readline mtl_in_filet)
		Glass_G=execute(readline mtl_in_filet)
		Glass_B=execute(readline mtl_in_filet)
		TintCloudiness=execute(readline mtl_in_filet)

		if Illum_Rz  !="" do
		(
			try(
		IllumR.value=IllumRz as float
			)
			catch()
		)
		
		if Illum_Gz  !="" do
		(
			try(
		IllumG.value=IllumGz as float)
		catch()
		)
		
		if Illum_Bz  !="" do
		(try
			(
		IllumR.value=IllumBz as float
			)
			catch()
		)
		
		close mtl_in_filet
	)
	
	
	on Illum_Diff pressed do
	(
		mtl_in_filet = openFile "T://SC_Cache//BaseOutput.txt" mode: "rt" --SET THIS TO TEMP LOCATION
		
		mtlname=execute(readline mtl_in_filet)
		Base_diff=execute(readline mtl_in_filet) 
		Base_ddna=execute(readline mtl_in_filet) 
		Base_detail=execute(readline mtl_in_filet)
		Base_spec=execute(readline mtl_in_filet)
		BaseB_diff=execute(readline mtl_in_filet)
		Base_disp=execute(readline mtl_in_filet)
		BaseB_ddna=execute(readline mtl_in_filet)
		BaseB_detail=execute(readline mtl_in_filet)
		BaseB_spec=execute(readline mtl_in_filet)
		Base_mask=execute(readline mtl_in_filet)
		Base_SpecR=execute(readline mtl_in_filet)
		Base_SpecG=execute(readline mtl_in_filet)
		Base_SpecB=execute(readline mtl_in_filet)
		BaseB_SpecR=execute(readline mtl_in_filet)
		BaseB_SpecG=execute(readline mtl_in_filet)
		BaseB_SpecB=execute(readline mtl_in_filet)
		Base_DiffR=execute(readline mtl_in_filet)
		Base_DiffG=execute(readline mtl_in_filet)
		Base_DiffB=execute(readline mtl_in_filet)
		Base2_DiffR=execute(readline mtl_in_filet)
		Base2_DiffG=execute(readline mtl_in_filet)
		Base2_DiffB=execute(readline mtl_in_filet)
		Base_BlendLayerTile=execute(readline mtl_in_filet)
		Base_WearBlendBase=execute(readline mtl_in_filet)
		Base_BlendFactor=execute(readline mtl_in_filet)
		Base_Gloss=execute(readline mtl_in_filet)
		Base2_Gloss=execute(readline mtl_in_filet)
		Base_Tile=execute(readline mtl_in_filet)
		Base_Detail_DiffScale=execute(readline mtl_in_filet)
		Base2_Detail_DiffScale=execute(readline mtl_in_filet)
		Base_Detail_Nrm=execute(readline mtl_in_filet)
		BaseB_Detail_Nrm=execute(readline mtl_in_filet)
		Base_Detail_Gloss=execute(readline mtl_in_filet)
		BaseB_Detail_Gloss=execute(readline mtl_in_filet)
		Base_TintR=execute(readline mtl_in_filet)
		Base_TintG=execute(readline mtl_in_filet)
		Base_TintB=execute(readline mtl_in_filet)
		Base2_TintR=execute(readline mtl_in_filet)
		Base2_TintG=execute(readline mtl_in_filet)
		Base2_TintB=execute(readline mtl_in_filet)
		Base_LayerGloss=execute(readline mtl_in_filet)
		Base2_LayerGloss=execute(readline mtl_in_filet)
		Base_Layer1UV=execute(readline mtl_in_filet)
		Base2_Layer2UV=execute(readline mtl_in_filet)
		LayerTF2=execute(readline mtl_in_filet)
		LayerTF1=execute(readline mtl_in_filet)
		DisplOutput=execute(readline mtl_in_filet)
		TintPallete_Base=execute(readline mtl_in_filet)
		TintPallete_Blend=execute(readline mtl_in_filet)
		Base_Detail_TileU=execute(readline mtl_in_filet)
		Base_Detail_TileV=execute(readline mtl_in_filet)
		Blend_Detail_TileU=execute(readline mtl_in_filet)
		Blend_Detail_TileV=execute(readline mtl_in_filet)
		IllumRz=execute(readline mtl_in_filet)
		IllumGz=execute(readline mtl_in_filet)
		IllumBz=execute(readline mtl_in_filet)
		Glow=execute(readline mtl_in_filet)
		Glass_R=execute(readline mtl_in_filet)
		Glass_G=execute(readline mtl_in_filet)
		Glass_B=execute(readline mtl_in_filet)
		TintCloudiness=execute(readline mtl_in_filet)

		if Base_DiffR  !="" do
		(
		IllumR.value=Base_DiffR as float
		)
		
		if Base_DiffG  !="" do
		(
		IllumG.value=Base_DiffG as float
		)
		
		if Illum_B  !="" do
		(
		IllumR.value=Base_DiffB as float
		)
		close mtl_in_filet
	)
	

	
	
	
	on btn_make pressed do
	(
		meditMaterials[activeMeditSlot]=VRayLightMtl ()
		meditMaterials[activeMeditSlot].color=color (Illum_R.value*255) (Illum_G.value*255) (Illum_B.value*255) 
		meditMaterials[activeMeditSlot].texmap=CompositeTexturemap()
		meditMaterials[activeMeditSlot].texmap.maplist[1]=VRayBitmap()
		meditMaterials[activeMeditSlot].texmap.maplist[1].HDRIMapName=(TexPath.text+TexMap_Diff.text)
		meditMaterials[activeMeditSlot].texmap.maplist[2]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap.maplist[2].color=color (Illum_R.value*255) (Illum_G.value*255) (Illum_B.value*255) 255
		meditMaterials[activeMeditSlot].texmap.blendmode[2]=5
		meditMaterials[activeMeditSlot].twoSided=true 
		meditMaterials[activeMeditSlot].compensate_exposure=true
		meditMaterials[activeMeditSlot].multiplier=(GlowAmount.value*10)
		meditMaterials[activeMeditSlot].name=(Name_MTL.text+"_mtl_"+Material_Name.text)
		meditMaterials[activeMeditSlot].displacement_texmap = VRayBitmap ()
		meditMaterials[activeMeditSlot].displacement_texmap.HDRIMapName=(TexPath.text+TexMap_Displ.text)
		meditMaterials[activeMeditSlot].displacement_multiplier = DisplacementAmount.value
		
		if chk_alpha.state==on do
		(
		
		meditMaterials[activeMeditSlot].opacity_multiplyColor=true
		meditMaterials[activeMeditSlot].opacity_texmap=VRayBitmap()
		meditMaterials[activeMeditSlot].opacity_texmap.HDRIMapName=(TexPath.text+TexMap_Diff.text)
		meditMaterials[activeMeditSlot].opacity_texmap.color_space=0
		meditMaterials[activeMeditSlot].opacity_texmap.alphaSource=0
		meditMaterials[activeMeditSlot].opacity_texmap.rgbOutput = 1
		meditMaterials[activeMeditSlot].opacity_texmap.monoOutput = 1
		meditMaterials[activeMeditSlot].opacity_texmap_on=on
		)
	)

)

CreateDialog SCGlow
