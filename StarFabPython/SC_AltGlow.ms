try (closeRolloutFloater Alt_Glow) catch()


rollout Alt_Glow "AlternativeGlow 0.0.1" width:794 height:300
(
	label 'lbl1' "Alternative Glow (Displace)" pos:[317,9] width:165 height:18 align:#left
	editText 'MtlName' "MTL:" pos:[38,25] width:195 height:20 align:#left
	editText 'Namez' "Name:" pos:[246,25] width:175 height:20 align:#left
	spinner 'DiffR' "R" pos:[500,32] width:73 height:16 range:[0,1,0] align:#left
	spinner 'DiffG' "G" pos:[600,32] width:73 height:16 range:[0,1,0] align:#left
	spinner 'DiffB' "B" pos:[695,32] width:74 height:16 range:[0,1,0] align:#left
	groupBox 'grp1' "Diff" pos:[482,11] width:305 height:47 align:#left
	spinner 'Spec_R' "R" pos:[500,83] width:73 height:16 range:[0,1,0] align:#left
	spinner 'Spec_G' "G" pos:[600,83] width:73 height:16 range:[0,1,0] align:#left
	spinner 'Spec_B' "B" pos:[695,83] width:74 height:16 range:[0,1,0] align:#left
	groupBox 'grp2' "Spec" pos:[482,62] width:305 height:47 align:#left
	spinner 'Illum_R' "R" pos:[500,137] width:73 height:16 range:[0,1,1e-10] align:#left
	spinner 'Illum_G' "G" pos:[600,137] width:73 height:16 range:[0,1,1e-10] align:#left
	spinner 'Illum_B' "B" pos:[695,137] width:74 height:16 range:[0,1,1e-10] align:#left
	groupBox 'grp3' "Emission" pos:[484,114] width:305 height:69 align:#left
	button 'UseDiff' "Use Diff" pos:[531,160] width:80 height:18 align:#left
	button 'UseIllum' "Use Illum" pos:[630,160] width:80 height:18 align:#left
	button 'MakeMat' "Import Material" pos:[25,261] width:130 height:28 align:#left
	button 'GMLTransfer' "Transfer from GML" pos:[85,5] width:130 height:16 align:#left
	groupBox 'ManinCorr' "Detail Texture Coord" pos:[598,189] width:193 height:101 align:#left
	groupBox 'grp5' "Main Texture Coord" pos:[398,189] width:196 height:101 align:#left
	editText 'DiffTex' "Diffuse" pos:[34,77] width:370 height:20 align:#right
	editText 'TexSpec' "Spec" pos:[39,123] width:390 height:20 align:#right
	editText 'TexDetail' "Detail" pos:[34,171] width:395 height:20 align:#right
	editText 'TexDDNA' "DDNA" pos:[34,99] width:395 height:20 align:#right
	editText 'TexDisp' "Displace" pos:[21,147] width:410 height:20 align:#right
	spinner 'GlowValue' "Glow" pos:[90,209] width:68 height:16 range:[0,1000,0] type:#float align:#left
	spinner 'HeightStr' "Displacement" pos:[222,209] width:98 height:16 range:[0,1000,0] align:#left
	spinner 'TileU' "Tile U" pos:[412,209] width:53 height:16 enabled:true range:[0,500,1] align:#left
	spinner 'TileV' "Tile V" pos:[505,210] width:54 height:16 range:[0,500,1] align:#left
	spinner 'DTile_U' "Tile U" pos:[616,210] width:53 height:16 enabled:true range:[0,500,1] align:#left
	spinner 'DTile_V' "Tile V" pos:[709,210] width:54 height:16 range:[0,500,1] align:#left
	spinner 'OffU' "Off U" pos:[410,249] width:56 height:16 enabled:true range:[0,500,0] align:#left
	spinner 'OffV' "Off V" pos:[501,250] width:57 height:16 range:[0,500,0] align:#left
	spinner 'D_OffU' "Off U" pos:[612,228] width:56 height:16 enabled:true range:[0,500,0] align:#left
	spinner 'D_OffV' "Off V" pos:[705,228] width:57 height:16 range:[0,500,0] align:#left
	spinner 'GlossInput' "Gloss" pos:[270,237] width:73 height:16 range:[0,255,1] align:#left
	spinner 'Scale_Diff' "Diff SC" pos:[604,250] width:57 height:16 range:[0,500,0.5] align:#left
	spinner 'Scale_Gloss' "Gloss SC" pos:[700,250] width:50 height:16 range:[0,500,0.5] align:#left
	spinner 'Scale_DDNA' "Norm SC" pos:[604,270] width:57 height:16 range:[0,500,0.5] align:#left
	editText 'TexturePath' "Texture Path" pos:[10,51] width:303 height:20 align:#left text:"D:/Star Citizen Master Texture/"
	checkbox  'UseAlpha' "Use Alpha"pos:[410,77] width:65 height:20 align:#right checked:off
	
	on GMLTransfer pressed do
	(
	
		mtl_in_filet = openFile "T://SC_Cache//BaseOutput.txt" mode: "rt" --SET THIS TO TEMP LOCATION
		
		mtlnamez=execute(readline mtl_in_filet)
		Base_diff=execute(readline mtl_in_filet) 
		Base_ddna=execute(readline mtl_in_filet) 
		Base_detail=execute(readline mtl_in_filet)
		Base_spec=execute(readline mtl_in_filet)
		BaseB_diff=execute(readline mtl_in_filet)
		Base_disp=execute(readline mtl_in_filet)
		BaseB_ddna=execute(readline mtl_in_filet)
		BaseB_detail=execute(readline mtl_in_filet)
		BaseB_spec=execute(readline mtl_in_filet)
		Base_mask=execute(readline mtl_in_filet)
		Base_SpecR=execute(readline mtl_in_filet)
		Base_SpecG=execute(readline mtl_in_filet)
		Base_SpecB=execute(readline mtl_in_filet)
		BaseB_SpecR=execute(readline mtl_in_filet)
		BaseB_SpecG=execute(readline mtl_in_filet)
		BaseB_SpecB=execute(readline mtl_in_filet)
		Base_DiffR=execute(readline mtl_in_filet)
		Base_DiffG=execute(readline mtl_in_filet)
		Base_DiffB=execute(readline mtl_in_filet)
		Base2_DiffR=execute(readline mtl_in_filet)
		Base2_DiffG=execute(readline mtl_in_filet)
		Base2_DiffB=execute(readline mtl_in_filet)
		Base_BlendLayerTile=execute(readline mtl_in_filet)
		Base_WearBlendBase=execute(readline mtl_in_filet)
		Base_BlendFactor=execute(readline mtl_in_filet)
		Base_Gloss=execute(readline mtl_in_filet)
		Base2_Gloss=execute(readline mtl_in_filet)
		Base_Tile=execute(readline mtl_in_filet)
		Base_Detail_DiffScale=execute(readline mtl_in_filet)
		Base2_Detail_DiffScale=execute(readline mtl_in_filet)
		Base_Detail_Nrm=execute(readline mtl_in_filet)
		BaseB_Detail_Nrm=execute(readline mtl_in_filet)
		Base_Detail_Gloss=execute(readline mtl_in_filet)
		BaseB_Detail_Gloss=execute(readline mtl_in_filet)
		Base_TintR=execute(readline mtl_in_filet)
		Base_TintG=execute(readline mtl_in_filet)
		Base_TintB=execute(readline mtl_in_filet)
		Base2_TintR=execute(readline mtl_in_filet)
		Base2_TintG=execute(readline mtl_in_filet)
		Base2_TintB=execute(readline mtl_in_filet)
		Base_LayerGloss=execute(readline mtl_in_filet)
		Base2_LayerGloss=execute(readline mtl_in_filet)
		Base_Layer1UV=execute(readline mtl_in_filet)
		Base2_Layer2UV=execute(readline mtl_in_filet)
		LayerTF2=execute(readline mtl_in_filet)
		LayerTF1=execute(readline mtl_in_filet)
		DisplOutput=execute(readline mtl_in_filet)
		TintPallete_Base=execute(readline mtl_in_filet)
		TintPallete_Blend=execute(readline mtl_in_filet)
		Base_Detail_TileU=execute(readline mtl_in_filet)
		Base_Detail_TileV=execute(readline mtl_in_filet)
		Blend_Detail_TileU=execute(readline mtl_in_filet)
		Blend_Detail_TileV=execute(readline mtl_in_filet)
		IllumR=execute(readline mtl_in_filet)
		IllumG=execute(readline mtl_in_filet)
		IllumB=execute(readline mtl_in_filet)
		GlowZ=execute(readline mtl_in_filet)
		Glass_R=execute(readline mtl_in_filet)
		Glass_G=execute(readline mtl_in_filet)
		Glass_B=execute(readline mtl_in_filet)
		TintCloudiness=execute(readline mtl_in_filet)
		Main_TileU=execute(readline mtl_in_filet)
		Main_TileV=execute(readline mtl_in_filet)
		Main_OFFU=execute(readline mtl_in_filet)
		Main_OFFV=execute(readline mtl_in_filet)
		BMain_TileU=execute(readline mtl_in_filet)
		BMain_TileV=execute(readline mtl_in_filet)
		BMain_OFFU=execute(readline mtl_in_filet)
		BMain_OFFV=execute(readline mtl_in_filet)
		Gloss_TileU=execute(readline mtl_in_filet)
		Gloss_TileV=execute(readline mtl_in_filet)
		Gloss_OFFU=execute(readline mtl_in_filet)
		Gloss_OFFV=execute(readline mtl_in_filet)
		BGloss_TileU=execute(readline mtl_in_filet)
		BGloss_TileV=execute(readline mtl_in_filet)
		BGloss_OFFU=execute(readline mtl_in_filet)
		BGloss_OFFV=execute(readline mtl_in_filet)
		Detail_OFFU=execute(readline mtl_in_filet)
		Detail_OFFV=execute(readline mtl_in_filet)
		BDetail_OFFU=execute(readline mtl_in_filet)
		BDetail_OFFV=execute(readline mtl_in_filet)

		if mtlnamez  !="" do
		(
		Namez.text=mtlnamez as string
		)
		--Texture
		if Base_diff  !="" do
		(
		DiffTex.text=Base_diff as string
		)
		
		if Base_ddna  !="" do
		(
		TexDDNA.text=Base_ddna as string
		)
		
		if Base_detail  !="" do
		(
		TexDetail.text=Base_detail as string
		)
		
		if Base_spec  !="" do
		(
		TexSpec.text=Base_spec as string
		)
		
		if Base_disp  !="" do
		(
		TexDisp.text=Base_disp as string
		)
		--Diff
		if Base_DiffR  !="" do
		(
		DiffR.value=Base_DiffR as float
		)
		
		if Base_DiffG  !="" do
		(
		DiffG.value=Base_DiffG as float
		)
		
		if Base_DiffB  !="" do
		(
		DiffB.value=Base_DiffB as float
		)
		--Specular
		if Base_SpecR  !="" do
		(
		Spec_R.value=Base_SpecR as float
		)
		
		if Base_SpecG  !="" do
		(
		Spec_G.value=Base_SpecG as float
		)
		
		if Base_SpecB  !="" do
		(
		Spec_B.value=Base_SpecB as float
		)
		
		--Illum
		if IllumR  !="" do
		(
		Illum_R.value=IllumR as float
		)
		
		if IllumG  !="" do
		(
		Illum_G.value=IllumG as float
		)
		
		if IllumB  !="" do
		(
		Illum_B.value=IllumB as float
		)		
		--Gloss
		if Base_Gloss  !="" do
		(
		GlossInput.value=Base_Gloss as float
		)
			---Displ
		if DisplOutput  !="" do
		(
		HeightStr.value=DisplOutput as float
		)
		if GlowZ  !="" do
		(
		GlowValue.value=GlowZ as float
		)
		if DisplOutput  !="" do
		(
		HeightStr.value=DisplOutput as float
		)
		
		if Main_TileU  !="" do
		(
		TileU.value=Main_TileU as float
		)
		if Main_TileV  !="" do
		(
		TileV.value=Main_TileV as float
		)
		if Main_OFFU  !="" do
		(
		OffU.value=Main_OFFU as float
		)
		if Main_OFFV  !="" do
		(
		OffV.value=Main_OFFV as float
		)	
		--Detail Coord
		if Base_Detail_TileU  !="" do
		(
		DTile_U.value=Base_Detail_TileU as float
		)
		if Base_Detail_TileV  !="" do
		(
		DTile_V.value=Base_Detail_TileV as float
		)
		if Detail_OFFU  !="" do
		(
		D_OffU.value=Detail_OFFU as float
		)
		if Detail_OFFV  !="" do
		(
		D_OffV.value=Detail_OFFV as float
		)			
		--detail scales
		if Base_Detail_DiffScale  !="" do
		(
		Scale_Diff.value=Base_Detail_DiffScale as float
		)
		if Base_Detail_Nrm  !="" do
		(
		Scale_DDNA.value=Base_Detail_Nrm as float
		)
		if Base_Detail_Gloss  !="" do
		(
		Scale_Gloss.value=Base_Detail_Gloss as float
		)	
		
		
		
		


		
		
		close mtl_in_filet
		
		
	)
	
	on UseIllum pressed do
	(
		mtl_in_filet = openFile "T://SC_Cache//BaseOutput.txt" mode: "rt" --SET THIS TO TEMP LOCATION
		
		mtlnamez=execute(readline mtl_in_filet)
		Base_diff=execute(readline mtl_in_filet) 
		Base_ddna=execute(readline mtl_in_filet) 
		Base_detail=execute(readline mtl_in_filet)
		Base_spec=execute(readline mtl_in_filet)
		BaseB_diff=execute(readline mtl_in_filet)
		Base_disp=execute(readline mtl_in_filet)
		BaseB_ddna=execute(readline mtl_in_filet)
		BaseB_detail=execute(readline mtl_in_filet)
		BaseB_spec=execute(readline mtl_in_filet)
		Base_mask=execute(readline mtl_in_filet)
		Base_SpecR=execute(readline mtl_in_filet)
		Base_SpecG=execute(readline mtl_in_filet)
		Base_SpecB=execute(readline mtl_in_filet)
		BaseB_SpecR=execute(readline mtl_in_filet)
		BaseB_SpecG=execute(readline mtl_in_filet)
		BaseB_SpecB=execute(readline mtl_in_filet)
		Base_DiffR=execute(readline mtl_in_filet)
		Base_DiffG=execute(readline mtl_in_filet)
		Base_DiffB=execute(readline mtl_in_filet)
		Base2_DiffR=execute(readline mtl_in_filet)
		Base2_DiffG=execute(readline mtl_in_filet)
		Base2_DiffB=execute(readline mtl_in_filet)
		Base_BlendLayerTile=execute(readline mtl_in_filet)
		Base_WearBlendBase=execute(readline mtl_in_filet)
		Base_BlendFactor=execute(readline mtl_in_filet)
		Base_Gloss=execute(readline mtl_in_filet)
		Base2_Gloss=execute(readline mtl_in_filet)
		Base_Tile=execute(readline mtl_in_filet)
		Base_Detail_DiffScale=execute(readline mtl_in_filet)
		Base2_Detail_DiffScale=execute(readline mtl_in_filet)
		Base_Detail_Nrm=execute(readline mtl_in_filet)
		BaseB_Detail_Nrm=execute(readline mtl_in_filet)
		Base_Detail_Gloss=execute(readline mtl_in_filet)
		BaseB_Detail_Gloss=execute(readline mtl_in_filet)
		Base_TintR=execute(readline mtl_in_filet)
		Base_TintG=execute(readline mtl_in_filet)
		Base_TintB=execute(readline mtl_in_filet)
		Base2_TintR=execute(readline mtl_in_filet)
		Base2_TintG=execute(readline mtl_in_filet)
		Base2_TintB=execute(readline mtl_in_filet)
		Base_LayerGloss=execute(readline mtl_in_filet)
		Base2_LayerGloss=execute(readline mtl_in_filet)
		Base_Layer1UV=execute(readline mtl_in_filet)
		Base2_Layer2UV=execute(readline mtl_in_filet)
		LayerTF2=execute(readline mtl_in_filet)
		LayerTF1=execute(readline mtl_in_filet)
		DisplOutput=execute(readline mtl_in_filet)
		TintPallete_Base=execute(readline mtl_in_filet)
		TintPallete_Blend=execute(readline mtl_in_filet)
		Base_Detail_TileU=execute(readline mtl_in_filet)
		Base_Detail_TileV=execute(readline mtl_in_filet)
		Blend_Detail_TileU=execute(readline mtl_in_filet)
		Blend_Detail_TileV=execute(readline mtl_in_filet)
		IllumR=execute(readline mtl_in_filet)
		IllumG=execute(readline mtl_in_filet)
		IllumB=execute(readline mtl_in_filet)
		Glow=execute(readline mtl_in_filet)
		Glass_R=execute(readline mtl_in_filet)
		Glass_G=execute(readline mtl_in_filet)
		Glass_B=execute(readline mtl_in_filet)
		TintCloudiness=execute(readline mtl_in_filet)
		Main_TileU=execute(readline mtl_in_filet)
		Main_TileV=execute(readline mtl_in_filet)
		Main_OFFU=execute(readline mtl_in_filet)
		Main_OFFV=execute(readline mtl_in_filet)
		BMain_TileU=execute(readline mtl_in_filet)
		BMain_TileV=execute(readline mtl_in_filet)
		BMain_OFFU=execute(readline mtl_in_filet)
		BMain_OFFV=execute(readline mtl_in_filet)
		Gloss_TileU=execute(readline mtl_in_filet)
		Gloss_TileV=execute(readline mtl_in_filet)
		Gloss_OFFU=execute(readline mtl_in_filet)
		Gloss_OFFV=execute(readline mtl_in_filet)
		BGloss_TileU=execute(readline mtl_in_filet)
		BGloss_TileV=execute(readline mtl_in_filet)
		BGloss_OFFU=execute(readline mtl_in_filet)
		BGloss_OFFV=execute(readline mtl_in_filet)
		Detail_OFFU=execute(readline mtl_in_filet)
		Detail_OFFV=execute(readline mtl_in_filet)
		BDetail_OFFU=execute(readline mtl_in_filet)
		BDetail_OFFV=execute(readline mtl_in_filet)
		
		if IllumR  !="" do
		(
		Illum_R.value=IllumR as float
		)
		
		if IllumG  !="" do
		(
		Illum_G.value=IllumG as float
		)
		
		if IllumB  !="" do
		(
		Illum_B.value=IllumB as float
		)
		
		close mtl_in_filet
		
	)
	
	on UseDiff pressed do
	(
		mtl_in_filet = openFile "T://SC_Cache//BaseOutput.txt" mode: "rt" --SET THIS TO TEMP LOCATION
		
		mtlnamez=execute(readline mtl_in_filet)
		Base_diff=execute(readline mtl_in_filet) 
		Base_ddna=execute(readline mtl_in_filet) 
		Base_detail=execute(readline mtl_in_filet)
		Base_spec=execute(readline mtl_in_filet)
		BaseB_diff=execute(readline mtl_in_filet)
		Base_disp=execute(readline mtl_in_filet)
		BaseB_ddna=execute(readline mtl_in_filet)
		BaseB_detail=execute(readline mtl_in_filet)
		BaseB_spec=execute(readline mtl_in_filet)
		Base_mask=execute(readline mtl_in_filet)
		Base_SpecR=execute(readline mtl_in_filet)
		Base_SpecG=execute(readline mtl_in_filet)
		Base_SpecB=execute(readline mtl_in_filet)
		BaseB_SpecR=execute(readline mtl_in_filet)
		BaseB_SpecG=execute(readline mtl_in_filet)
		BaseB_SpecB=execute(readline mtl_in_filet)
		Base_DiffR=execute(readline mtl_in_filet)
		Base_DiffG=execute(readline mtl_in_filet)
		Base_DiffB=execute(readline mtl_in_filet)
		Base2_DiffR=execute(readline mtl_in_filet)
		Base2_DiffG=execute(readline mtl_in_filet)
		Base2_DiffB=execute(readline mtl_in_filet)
		Base_BlendLayerTile=execute(readline mtl_in_filet)
		Base_WearBlendBase=execute(readline mtl_in_filet)
		Base_BlendFactor=execute(readline mtl_in_filet)
		Base_Gloss=execute(readline mtl_in_filet)
		Base2_Gloss=execute(readline mtl_in_filet)
		Base_Tile=execute(readline mtl_in_filet)
		Base_Detail_DiffScale=execute(readline mtl_in_filet)
		Base2_Detail_DiffScale=execute(readline mtl_in_filet)
		Base_Detail_Nrm=execute(readline mtl_in_filet)
		BaseB_Detail_Nrm=execute(readline mtl_in_filet)
		Base_Detail_Gloss=execute(readline mtl_in_filet)
		BaseB_Detail_Gloss=execute(readline mtl_in_filet)
		Base_TintR=execute(readline mtl_in_filet)
		Base_TintG=execute(readline mtl_in_filet)
		Base_TintB=execute(readline mtl_in_filet)
		Base2_TintR=execute(readline mtl_in_filet)
		Base2_TintG=execute(readline mtl_in_filet)
		Base2_TintB=execute(readline mtl_in_filet)
		Base_LayerGloss=execute(readline mtl_in_filet)
		Base2_LayerGloss=execute(readline mtl_in_filet)
		Base_Layer1UV=execute(readline mtl_in_filet)
		Base2_Layer2UV=execute(readline mtl_in_filet)
		LayerTF2=execute(readline mtl_in_filet)
		LayerTF1=execute(readline mtl_in_filet)
		DisplOutput=execute(readline mtl_in_filet)
		TintPallete_Base=execute(readline mtl_in_filet)
		TintPallete_Blend=execute(readline mtl_in_filet)
		Base_Detail_TileU=execute(readline mtl_in_filet)
		Base_Detail_TileV=execute(readline mtl_in_filet)
		Blend_Detail_TileU=execute(readline mtl_in_filet)
		Blend_Detail_TileV=execute(readline mtl_in_filet)
		IllumR=execute(readline mtl_in_filet)
		IllumG=execute(readline mtl_in_filet)
		IllumB=execute(readline mtl_in_filet)
		Glow=execute(readline mtl_in_filet)
		Glass_R=execute(readline mtl_in_filet)
		Glass_G=execute(readline mtl_in_filet)
		Glass_B=execute(readline mtl_in_filet)
		TintCloudiness=execute(readline mtl_in_filet)
		Main_TileU=execute(readline mtl_in_filet)
		Main_TileV=execute(readline mtl_in_filet)
		Main_OFFU=execute(readline mtl_in_filet)
		Main_OFFV=execute(readline mtl_in_filet)
		BMain_TileU=execute(readline mtl_in_filet)
		BMain_TileV=execute(readline mtl_in_filet)
		BMain_OFFU=execute(readline mtl_in_filet)
		BMain_OFFV=execute(readline mtl_in_filet)
		Gloss_TileU=execute(readline mtl_in_filet)
		Gloss_TileV=execute(readline mtl_in_filet)
		Gloss_OFFU=execute(readline mtl_in_filet)
		Gloss_OFFV=execute(readline mtl_in_filet)
		BGloss_TileU=execute(readline mtl_in_filet)
		BGloss_TileV=execute(readline mtl_in_filet)
		BGloss_OFFU=execute(readline mtl_in_filet)
		BGloss_OFFV=execute(readline mtl_in_filet)
		Detail_OFFU=execute(readline mtl_in_filet)
		Detail_OFFV=execute(readline mtl_in_filet)
		BDetail_OFFU=execute(readline mtl_in_filet)
		BDetail_OFFV=execute(readline mtl_in_filet)
		
		if Base_DiffR  !="" do
		(
		Illum_R.value=Base_DiffR as float
		)
		
		if Base_DiffG  !="" do
		(
		Illum_G.value=Base_DiffG as float
		)
		
		if Base_DiffB  !="" do
		(
		Illum_B.value=Base_DiffB as float
		)
		
		close mtl_in_filet
	)
	
	on MakeMat pressed do
	(
		
		if TexDDNA.text !="" do
		(
			SCNormalString = TexDDNA.text
			GlossPath=substring SCNormalString 1 (SCNormalString.count-4)+"_glossMap.tif"
		)
		if TexDetail.text !="" do
		(
			NDetailString = TexDetail.text
			NormalDetailMap=substring NDetailString 1 (NDetailString.Count-7)+"dna.tif"
			NormalDetailGlossMap=substring NDetailString 1 (NDetailString.Count-7)+"dna_glossMap.tif"
		)

		if DiffTex.text != "" do
		(
			meditMaterials[activeMeditSlot]=VRayBlendMtl()
			meditMaterials[activeMeditSlot].baseMtl=VRayMtl()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse= CompositeTexturemap() 
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection= CompositeTexturemap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness= CompositeTexturemap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1]=CompositeTexturemap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.opacity[1]=((GlossInput.value/255)*100)
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump=VRayNormalMap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump_multiplier =100.0
			meditMaterials[activeMeditSlot].name=(MtlName.text+"_mtl_"+Namez.text)
			meditMaterials[activeMeditSlot].basemtl.selfIllumination_multiplier=(GlowValue.value*10)

			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1]=VRayBitmap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1].HDRIMapName=(TexturePath.text+DiffTex.text)
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1].alphaSource=2
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1].coords.U_Offset = OffU.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1].coords.V_Offset = OffV.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1].coords.U_Tiling = TileU.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1].coords.V_Tiling = TileV.value
			
			meditMaterials[activeMeditSlot].baseMtl.diffuse=color (DiffR.value *255) (DiffG.value *255) (DiffB.value *255) 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[3]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[3].color=color (DiffR.value *255) (DiffG.value *255) (DiffB.value *255) 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.blendMode[3]=5
			
			meditMaterials[activeMeditSlot].baseMtl.texmap_opacity=VRayBitmap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_opacity.HDRIMapName=(TexturePath.text+DiffTex.text)
			meditMaterials[activeMeditSlot].baseMtl.texmap_opacity.color_space=0
			meditMaterials[activeMeditSlot].baseMtl.texmap_opacity.alphaSource=0
			meditMaterials[activeMeditSlot].baseMtl.texmap_opacity.rgbOutput = 1
			meditMaterials[activeMeditSlot].baseMtl.texmap_opacity.monoOutput = 1
			meditMaterials[activeMeditSlot].baseMtl.texmap_opacity.coords.U_Offset = OffU.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_opacity.coords.V_Offset = OffV.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_opacity.coords.U_Tiling = TileU.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_opacity.coords.V_Tiling = TileV.value
			meditMaterials[activeMeditSlot].baseMtl.option_opacityMode=0
			
			meditMaterials[activeMeditSlot].coatMtl[1]=VRayLightMtl ()
			meditMaterials[activeMeditSlot].coatMtl[1].color=color (Illum_R.value*255) (Illum_G.value*255) (Illum_B.value*255) 
			meditMaterials[activeMeditSlot].coatMtl[1].texmap=CompositeTexturemap()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap.maplist[1]=meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[1]
			meditMaterials[activeMeditSlot].coatMtl[1].texmap.maplist[2]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap.maplist[2].color=color (Illum_R.value*255) (Illum_G.value*255) (Illum_B.value*255) 255
			meditMaterials[activeMeditSlot].coatMtl[1].texmap.blendmode[2]=5
			meditMaterials[activeMeditSlot].coatMtl[1].twoSided=true 
			meditMaterials[activeMeditSlot].coatMtl[1].compensate_exposure=true
			meditMaterials[activeMeditSlot].coatMtl[1].multiplier=(GlowValue.value*10)
			meditMaterials[activeMeditSlot].coatMtl[1].name=(MtlName.text+"_mtl_"+Namez.text+"LightLayer")

			meditMaterials[activeMeditSlot].coatMtl[1].opacity_multiplyColor=true
			meditMaterials[activeMeditSlot].coatMtl[1].opacity_texmap=VRayBitmap()
			meditMaterials[activeMeditSlot].coatMtl[1].opacity_texmap.HDRIMapName=(TexturePath.text+DiffTex.text)
			meditMaterials[activeMeditSlot].coatMtl[1].opacity_texmap.color_space=0
			meditMaterials[activeMeditSlot].coatMtl[1].opacity_texmap.alphaSource=1
			meditMaterials[activeMeditSlot].coatMtl[1].opacity_texmap.rgbOutput = 1
			meditMaterials[activeMeditSlot].coatMtl[1].opacity_texmap.monoOutput = 1
			meditMaterials[activeMeditSlot].coatMtl[1].opacity_texmap_on=on
			meditMaterials[activeMeditSlot].coatMtl[1].opacity_texmap.coords.U_Offset = OffU.value
			meditMaterials[activeMeditSlot].coatMtl[1].opacity_texmap.coords.V_Offset = OffV.value
			meditMaterials[activeMeditSlot].coatMtl[1].opacity_texmap.coords.U_Tiling = TileU.value
			meditMaterials[activeMeditSlot].coatMtl[1].opacity_texmap.coords.V_Tiling = TileV.value
			
			if UseAlpha.state == true do
			(
				meditMaterials[activeMeditSlot].texmap_blend[1]=VRayBitmap()
				meditMaterials[activeMeditSlot].texmap_blend[1].HDRIMapName=(TexturePath.text+DiffTex.text)
				meditMaterials[activeMeditSlot].texmap_blend[1].color_space=0.0
				meditMaterials[activeMeditSlot].texmap_blend[1].alphaSource=1
				meditMaterials[activeMeditSlot].texmap_blend[1].rgbOutput=1
				meditMaterials[activeMeditSlot].texmap_blend[1].monoOutput=1
				meditMaterials[activeMeditSlot].texmap_blend[1].coords.U_Tiling = TileU.value
				meditMaterials[activeMeditSlot].texmap_blend[1].coords.V_Tiling = TileV.value
				meditMaterials[activeMeditSlot].texmap_blend[1].coords.U_Offset = OffU.value
				meditMaterials[activeMeditSlot].texmap_blend[1].coords.V_Offset = OffV.value
			)
			
			if UseAlpha.state == false do
			(
				meditMaterials[activeMeditSlot].texmap_blend[1]=VRayBitmap()
				meditMaterials[activeMeditSlot].texmap_blend[1].HDRIMapName=(TexturePath.text+DiffTex.text)
				meditMaterials[activeMeditSlot].texmap_blend[1].color_space=0
				meditMaterials[activeMeditSlot].texmap_blend[1].coords.U_Tiling = TileU.value
				meditMaterials[activeMeditSlot].texmap_blend[1].coords.V_Tiling = TileV.value
				meditMaterials[activeMeditSlot].texmap_blend[1].coords.U_Offset = OffU.value
				meditMaterials[activeMeditSlot].texmap_blend[1].coords.V_Offset = OffV.value
			)

		)

		if TexDisp.text != "" do		
		(
			meditMaterials[activeMeditSlot].baseMtl.texmap_displacement=VRayBitmap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_displacement.HDRIMapName=(TexturePath.text+TexDisp.text)
			meditMaterials[activeMeditSlot].baseMtl.texmap_displacement.color_space=0
			meditMaterials[activeMeditSlot].baseMtl.texmap_displacement_multiplier = (HeightStr.value*100)
			meditMaterials[activeMeditSlot].baseMtl.texmap_displacement.coords.U_Offset = OffU.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_displacement.coords.V_Offset = OffV.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_displacement.coords.U_Tiling = TileU.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_displacement.coords.V_Tiling = TileV.value
			
			meditMaterials[activeMeditSlot].coatMtl[1].displacement_texmap = VRayBitmap ()
			meditMaterials[activeMeditSlot].coatMtl[1].displacement_texmap.HDRIMapName=(TexturePath.text+TexDisp.text)
			meditMaterials[activeMeditSlot].coatMtl[1].displacement_multiplier = (HeightStr.value*100)
			meditMaterials[activeMeditSlot].coatMtl[1].displacement_texmap.coords.U_Offset = OffU.value
			meditMaterials[activeMeditSlot].coatMtl[1].displacement_texmap.coords.V_Offset = OffV.value
			meditMaterials[activeMeditSlot].coatMtl[1].displacement_texmap.coords.U_Tiling = TileU.value
			meditMaterials[activeMeditSlot].coatMtl[1].displacement_texmap.coords.V_Tiling = TileV.value
			
		)
		
		if TexDetail.text!="" do
		(
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2]=CompositeTextureMap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].maplist[1].rewireR=0
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].maplist[1].rewireG=10
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].maplist[1].rewireB=10
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].maplist[1].rewireA=9
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].maplist[1].map=VRayBitmap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].maplist[1].map.HDRIMapName=(TexturePath.text+TexDetail.text)
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].maplist[1].map.color_space=0
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].maplist[1].map.coords.U_Offset = D_OffU.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].maplist[1].map.coords.V_Offset = D_OffV.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].maplist[1].map.coords.U_Tiling = DTile_U.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].maplist[1].map.coords.V_Tiling = DTile_V.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].blendMode[1]=0
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.opacity[2]=(Scale_Diff.value *100)
		
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].maplist[2]=RGB_Tint()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].blendMode[2]=23
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.blendMode[2]=5	
			
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2]=CompositeTextureMap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].rewireR=10
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].rewireG=10
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].rewireB=2
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].rewireA=9
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map=VRayBitmap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.HDRIMapName=(TexturePath.text+TexDetail.text)
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.color_space=0.0
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.coords.U_Offset = D_OffU.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.coords.V_Offset = D_OffV.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.coords.U_Tiling = DTile_U.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.coords.V_Tiling = DTile_V.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].blendMode[1]=5
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].blendMode[1]=0
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].opacity[2]=(Scale_Gloss.value *100)
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[2]=RGB_Tint()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].blendMode[2]=23
			
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map=CompositeTextureMap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map.maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map.maplist[1].rewireR=1
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map.maplist[1].rewireG=3
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map.maplist[1].rewireB=9
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map.maplist[1].rewireA=3
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map.maplist[1].map=VRayBitmap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map.maplist[1].map.HDRIMapName=(TexturePath.text+TexDetail.text)
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map.maplist[1].map.color_space=0
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map.maplist[1].map.alphaSource=0
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map.maplist[1].map.coords.U_Offset = D_OffU.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map.maplist[1].map.coords.V_Offset = D_OffV.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map.maplist[1].map.coords.U_Tiling =  DTile_U.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map.maplist[1].map.coords.V_Tiling =  DTile_V.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map_multiplier=Scale_DDNA.value
			
		)
		
		if TexSpec.text!="" do
		(
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[1]=VRayBitmap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[1].HDRIMapName=(TexturePath.text+TexSpec.text)
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[1].color_space=0
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[1].coords.U_Offset = OffU.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[1].coords.V_Offset = OffV.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[1].coords.U_Tiling = TileU.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[1].coords.V_Tiling = TileV.value
		)
		if TexDDNA.text!="" do
		(
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1]=VRayBitmap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].HDRIMapName=(TexturePath.text+GlossPath)
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].color_space=0
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Offset = OffU.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Offset = OffV.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling = TileU.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling = TileV.value
			
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map=VRayBitmap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map.HDRIMapName=(TexturePath.text+TexDDNA.text)
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map.color_space=0
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map.coords.U_Offset = OffU.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map.coords.V_Offset = OffV.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map.coords.U_Tiling = TileU.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map.coords.V_Tiling = TileV.value
		)
		if TexDDNA.text=="" do
		(
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[3]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[3].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].blendMode[3]=5
		)
		
		
		
		
		
		
		
		
	)
)




createDialog Alt_Glow 