rollout TintNameAdjuster "Tint Name Adjuster" width:355 height:107
(
	button 'BaseAdd' "Add to Base Mat" pos:[26,65] width:117 height:27 align:#left
	button 'BlendAdd' "Add to Coat 1 Mat" pos:[196,65] width:117 height:27 align:#left
	editText 'NameAdd' "Name Additive" pos:[75,8] width:163 height:21 align:#left
	editText 'Numberz' "Tint Number" pos:[75,30] width:163 height:21 align:#left
	
	on BaseAdd pressed do
	(		try 
	(
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5]=undefined
		meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4]=undefined
		meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3]=undefined
		
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5]=ColorCorrection()
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5].color=color 255 255 255 255
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5].lightnessMode = 1
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5].gammaRGB = (1 / 2.2)
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5].name="TINT_"+NameAdd.text+Numberz.text
		
		meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4]=ColorCorrection()
		meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4].color=color 255 255 255 255
		meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4].lightnessMode = 1
		meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4].gammaRGB = (1 / 2.2)
		meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4].name="TINT_GLOSS_"+NameAdd.text+Numberz.text
		
		meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3]=ColorCorrection()
		meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3].color=color 255 255 255 255
		meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3].lightnessMode = 1
		meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3].gammaRGB = (1 / 2.2)
		meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3].name="TINT_SPEC_"+NameAdd.text+Numberz.text
		
		messagebox ("Tint Renamed to:"+meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5].name)
		
	)
	catch (messageBox "Not Correct Material")
	)
	on BlendAdd pressed do
	(try
	(
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5]=undefined
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4]=undefined
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3]=undefined
		
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5]=ColorCorrection()
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5].color=color 255 255 255 255
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5].lightnessMode = 1
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5].gammaRGB = (1 / 2.2)
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5].name="TINT_"+NameAdd.text+Numberz.text
		
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4]=ColorCorrection()
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4].color=color 255 255 255 255
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4].lightnessMode = 1
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4].gammaRGB = (1 / 2.2)
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4].name="TINT_GLOSS_"+NameAdd.text+Numberz.text
		
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3]=ColorCorrection()
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3].color=color 255 255 255 255
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3].lightnessMode = 1
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3].gammaRGB = (1 / 2.2)
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3].name="TINT_SPEC_"+NameAdd.text+Numberz.text
		
		messagebox ("Tint Renamed to:"+meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5].name)
		
	)
	catch (messageBox "Not Correct Material or Missing Coat layer")
	)
)

createDialog TintNameAdjuster 
