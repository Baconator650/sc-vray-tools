try (closeRolloutFloater TintAdder) catch()


rollout TintAdder "SC Tint Applicator" width:215 height:218
(
	button 'T1_L1' "Layer 1" pos:[22,60] width:77 height:22 align:#left
	button 'T1_L2' "Layer 2" pos:[110,61] width:77 height:22 align:#left
	label 'lbl1' "SC Tint Adder" pos:[80,8] width:70 height:15 align:#left
	label 'Ta' "Tint 1" pos:[93,43] width:65 height:15 align:#left
	button 'T2_L1' "Layer 1" pos:[22,112] width:77 height:22 align:#left
	button 'T2_L2' "Layer 2" pos:[110,113] width:77 height:22 align:#left
	label 'lbl3' "Tint 2" pos:[93,95] width:65 height:15 align:#left
	button 'T3_L1' "Layer 1" pos:[22,167] width:77 height:22 align:#left
	button 'T3_L2' "Layer 2" pos:[110,168] width:77 height:22 align:#left
	label 'lbl4' "Tint 3" pos:[93,150] width:65 height:15 align:#left
	
	on T1_L1 pressed do
	(
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.blendMode[4]=5
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.opacity[3]=50.0
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4].name="DiffuseLayer"
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5].lightnessMode = 1
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.blendMode[5]=5
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5].name="TINT_1"
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].blendMode[4]=5
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4].name="TINT_GLOSS_1"
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3].name="TINT_SPEC_1"
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.blendMode[3]=15
	)
	
	on T1_L2 pressed do
	(
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.blendMode[4]=5
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.opacity[3]=50.0
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4].name="DiffuseLayer"
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5].lightnessMode = 1
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.blendMode[5]=5
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5].name="TINT_1"
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].blendMode[4]=5
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4].name="TINT_GLOSS_1"
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3].name="TINT_SPEC_1"
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.blendMode[3]=15
	)
	
	on T2_L1 pressed do
	(
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.blendMode[4]=5
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.opacity[3]=50.0
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4].name="DiffuseLayer"
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5].lightnessMode = 1
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.blendMode[5]=5
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5].name="TINT_2"
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].blendMode[4]=5
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4].name="TINT_GLOSS_2"
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3].name="TINT_SPEC_2"
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.blendMode[3]=15
	)
	
	on T2_L2 pressed do
	(
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.blendMode[4]=5
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.opacity[3]=50.0
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4].name="DiffuseLayer"
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5].lightnessMode = 1
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.blendMode[5]=5
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5].name="TINT_2"
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].blendMode[4]=5
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4].name="TINT_GLOSS_2"
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3].name="TINT_SPEC_2"
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.blendMode[3]=15
	)	
	
	on T3_L1 pressed do
	(
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.blendMode[4]=5
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.opacity[3]=50.0
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4].name="DiffuseLayer"
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5].lightnessMode = 1
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.blendMode[5]=5
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[5].name="TINT_3"
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].blendMode[4]=5
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[4].name="TINT_GLOSS_3"
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[3].name="TINT_SPEC_3"
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.blendMode[3]=15
	)
	
	on T3_L2 pressed do
	(
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.blendMode[4]=5
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.opacity[3]=50.0
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4].name="DiffuseLayer"
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5].lightnessMode = 1
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.blendMode[5]=5
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[5].name="TINT_3"
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].blendMode[4]=5
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[4].name="TINT_GLOSS_3"
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3].color=color 255 255 255 255
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3].gammaRGB = (1 / 2.2)
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[3].name="TINT_SPEC_3"
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.blendMode[3]=15
	)	
	
)

CreateDialog TintAdder