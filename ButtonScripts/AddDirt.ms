
try (closeRolloutFloater DirtAdd) catch()


rollout DirtAdd "Legacy Dirt Add Tool" width:300 height:147
(
	label 'lbl1' "Legacy Dirt Add" pos:[109,8] width:87 height:17 align:#left
	button 'BTN_Dirt' "Add Dirt Layer" pos:[49,40] width:197 height:50 align:#left
	label 'lbl2' "Tool to add a Dirt layer to any Vray Blend with 2 layers and VrayMtl. Will occupy Composite Layer in the diffuse map and be placed in layers 6 and 7." pos:[14,97] width:275 height:45 align:#left
	
	on BTN_Dirt pressed do
	(
		if iskindof meditMaterials[activeMeditSlot] Vraymtl do
		(
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[6]=ColorCorrection()
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[6].name="DIRTCOLOR"
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[6].color= color 73 53 26 255
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[7]=meditMaterials[activeMeditSlot].texmap_diffuse.maplist[6]
			--DirtMask
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[7]=CompositeMap()
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].maplist[1]=VRayDirt()
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].maplist[1].radius=10000
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].maplist[1].occluded_color = color 255 255 255
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].maplist[1].unoccluded_color= color 0 0 0
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].maplist[1].affect_alpha = on
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].maplist[1].mode = 0
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].maplist[1].Distribution=.1
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].maplist[1].falloff=0.7
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].maplist[1].subdivs=12
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].maplist[1].radius_shortmap=Noise ()
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].maplist[1].radius_shortmap.size=.005
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].opacity[1]=0.0
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[6]=CompositeMap()
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[6].maplist[1]=Noise()
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[6].opacity[1]=0.0
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[6].maplist[1].size=0.3
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[6].maplist[1].thresholdHigh = 1
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[6].maplist[1].color1 = color 255 255 255
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[6].maplist[1].color2 = color 0 0 0
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK"
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[6].name="DIRT_INNER_MASK"

		)

		if iskindof meditMaterials[activeMeditSlot] VRayBlendMtl do	
		(
			if iskindof meditMaterials[activeMeditSlot].baseMtl VRayMtl do
			(
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[6]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[6].name="DIRTCOLOR"
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[6].color= color 73 53 26 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[7]=meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[6]
			--DirtMask
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7]=CompositeMap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].maplist[1]=VRayDirt()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].maplist[1].radius=10000
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].maplist[1].occluded_color = color 255 255 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].maplist[1].unoccluded_color= color 0 0 0
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].maplist[1].affect_alpha = on
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].maplist[1].mode = 0
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].maplist[1].Distribution=.1
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].maplist[1].falloff=0.7
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].maplist[1].subdivs=12
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].maplist[1].radius_shortmap=Noise ()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].maplist[1].radius_shortmap.size=.005
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].opacity[1]=0.0
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6]=CompositeMap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6].maplist[1]=Noise()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6].opacity[1]=0.0
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6].maplist[1].size=0.3
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6].maplist[1].thresholdHigh = 1
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6].maplist[1].color1 = color 255 255 255
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6].maplist[1].color2 = color 0 0 0
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK"
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6].name="DIRT_INNER_MASK"
			)
			
			if iskindof meditMaterials[activeMeditSlot].coatMtl[1] VRayMtl do
			(
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[7]=meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7]
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[6]=meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6]
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[6]=meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[6]
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[7]=meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[6]
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK"
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[6].name="DIRT_INNER_MASK" 
			)

		
			
		)
	)
)
createDialog DirtAdd