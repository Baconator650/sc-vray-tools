try (closeRolloutFloater SC_SHIPLayerSetup) catch()

rollout SC_SHIPLayerSetup "Ship Layer Setup" width:500 height:200
(


	button 'CreateLayer' "Create Layers" pos:[180,139] width:150 height:33 toolTip:"Creates new layers to help organize ship models" align:#left
	editText 'ShipName' "Ship Name" pos:[37,76] width:385 height:20 align:#left
	
	on CreateLayer pressed do
	(
		
		LayerManager.newLayerFromName (Shipname.text + "_LIGHTS_STANDARD")
		LayerManager.newLayerFromName (Shipname.text + "_LIGHTS_AUXILARY")
		LayerManager.newLayerFromName (Shipname.text + "_LIGHTS_EMERGENCY")
		LayerManager.newLayerFromName (Shipname.text + "_Helpers")
		LayerManager.newLayerFromName (Shipname.text + "_DAMAGE")
		LayerManager.newLayerFromName (Shipname.text + "_ANIMATION TOOLS")
		LayerManager.newLayerFromName (Shipname.text + "_Flight Controls")
		LayerManager.newLayerFromName (Shipname.text + "_LandingGear")				
	)
	
)

CreateDialog SC_SHIPLayerSetup
