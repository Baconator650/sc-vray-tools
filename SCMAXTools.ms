try (closeRolloutFloater SC_VRayTools) catch()

rollout SC_VRayTools "SC VRay Tools Alpha 0.0.1" width:600 height:629
(

	bitmap 'bmp2' "Bitmap" pos:[0,0] width:600 height:131 align:#left filename:"StarFabPython/SCToolset.png"
	button 'Load_LightSTD' "Light Loader Standard" pos:[442,264] width:150 height:33 toolTip:"" align:#left enabled:false 
	button 'Load_LightVray' "Light Loader VRay" pos:[442,168] width:150 height:33 toolTip:"" align:#left
	groupBox 'grp1' "Material Tools" pos:[23,141] width:370 height:293 align:#left
	bitmap 'bmp3' "Bitmap" pos:[478,437] width:82 height:82 align:#left filename:"StarFabPython/Logo_Community_logo.tiff"
	groupBox 'grp2' "Lights" pos:[429,143] width:168 height:282 align:#left
	label 'lbl1' "Load VrayBitmaps into the light texture. No gamma override needed." pos:[444,220] width:145 height:44 align:#left
	label 'lbl2' "Load Standard Bitmaps. Will need to have gamma override." pos:[441,312] width:150 height:37 align:#left
	button 'btn_gamma' "gamma override" pos:[271,446] width:116 height:23 align:#left
	label 'lbl3' "****Warning!! Will override all bitmap's gamma in current scene.****" pos:[255,469] width:143 height:45 align:#left
	groupBox 'grp4' "Other Tools" pos:[18,435] width:386 height:185 align:#left
	button 'btn_Pivot' "Pivot Align to Parent" pos:[37,462] width:70 height:33 align:#left
	button 'btn_MatLoad' "Lib Aid" pos:[128,463] width:93 height:33 align:#left 
--Materials Tools Left Side
	button 'Load_MTL' "General MTL Loader [A]" pos:[43,159] width:150 height:33 toolTip:"" align:#left
	button 'Load_WDAE' "WDA BW [A]" pos:[43,193] width:150 height:33 toolTip:"" align:#left
	button 'Load_WDAC' "WDA Clothing [A]" pos:[43,227] width:150 height:33 toolTip:"" align:#left
	button 'Load_Glass' "Glass" pos:[43,260] width:150 height:33 toolTip:"" align:#left
	button 'Load_Tint' "Tint Loader [A]" pos:[43,293] width:150 height:33 toolTip:"" align:#left
	button 'Load_Tintrenamer' "Tint Renamer" pos:[43,326] width:150 height:33 toolTip:"" align:#left
--Materials Tools Right Side
	button 'Load_Layer' "Layer Loader (WDA)" pos:[232,159] width:150 height:33 toolTip:"" align:#left
	button 'btn_Stencil' "Stencil Decal" pos:[232,193] width:150 height:33 toolTip:"" align:#left 
	button 'btn_camo' "Add Camo" pos:[232,227] width:150 height:33 toolTip:"" align:#left enabled:false 
	button 'btn_dirtren' "Dirt Renamer (EXT/INT)" pos:[232,260] width:150 height:33 toolTip:"" align:#left
	button 'ControlCube' "WD Control Cube" pos:[232,335] width:150 height:33 toolTip:"" align:#left
	
--Load Tools
		on btn_dirtren pressed do
		(
			filein ("StarFabPython/SC_DirtRenamer.ms")
		)

		on Load_Tintrenamer pressed do
	(
		fileIn("StarFabPython/ButtonScripts/TintNameAdjuster.ms")
	)
	
	on Load_Tint pressed do
	(
		fileIn("StarFabPython/SC_TintLoader.ms")
	)
	
	
	on Load_MTL pressed do
	(
		fileIn("StarFabPython/SC_MTL_Loader_V2.ms")
	)
	
		on Load_WDAE pressed do
	(
		fileIn("StarFabPython/SC_WDA_Equipment_Importer.ms")
	)
	
	on Load_WDAC pressed do
	(
		fileIn("StarFabPython/SC_WDA_Clothing_Importer.ms")
	)
	
	on Load_WDAS pressed do
	(
		fileIn("StarFabPython/SC_WDA_ShipComp_Importer.ms")
	)
	
	on Load_POM pressed do
	(
		fileIn("StarFabPython/SC_POM.ms")
	)
	
	on Load_Glass pressed do
	(
		fileIn("StarFabPython/SC_Glass.ms")
	)
	
	on Load_Layer pressed do
	(
		fileIn("StarFabPython/SC_Layer_Importer.ms")
	)
	
	on Load_LightSTD pressed do
	(
		fileIn("SC_Light_load_V2_STD.ms")
	)
		
	on Load_LightVray pressed do
	(
		fileIn("SC_Light_load_V2_Vray.ms")
	)
	
	on Load_Decal pressed do
	(
		fileIn("StarFabPython/SC_Decal.ms")
	)
	on btn_glow pressed do
	(
		fileIn("StarFabPython/SC_glow.ms")
	)
	
	on btn_camo pressed do
	(
		fileIn("StarFabPython/ButtonScripts/CamoAdd.ms")
	)
	
	on btn_gamma pressed do
	(
		for tmap in (getClassInstances BitmapTexture) where (tmap.bitmap.inputGamma == #default) do
			(
				tmap.bitmap = openBitmap tmap.bitmap.filename gamma:1.0
			)	
			
	)
	
	on btn_Pivot pressed do
	(
		fileIn("StarFabPython/ButtonScripts/PivotAlign2Parent.ms")
	)
	
	on btn_MatLoad pressed do
	(
		filein("StarFabPython/ButtonScripts/MatAirLoad.ms")
	)
	
	on btn_Stencil pressed do
	(
		filein("StarFabPython/SC_Stencil_Decal.ms")
	)
	
	on Load_AltGlow pressed do
	(
		filein("StarFabPython/SC_AltGlow.ms")
	)

	on ControlCube pressed do
	(
		
		filein ("StarFabPython/SC_WD_ControlCube.ms")
	)
)
CreateDialog SC_VRayTools
